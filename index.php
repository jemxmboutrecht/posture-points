<?php

session_start();

if ( isset($_GET['file_list']) ) include 'file_list.php';

$time_start = microtime(true);
include 'autoloader.php';

$app = new base\controllers\ApplicationController(__DIR__);

$time_end = microtime(true);
$execution_time = ($time_end - $time_start) / 1000;

if ( \base\controllers\ApplicationController::STATE == 'DEVELOPMENT' ) {
    //\base\controllers\ApplicationController::showper();
}
