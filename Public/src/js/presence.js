( function( $ ) {

    $('button#print').click(function() {
        print( $(this).parent('div.presence-container').children('div.lists') );
    });

    function print( divEl ) {
        var printHtml = divEl.html(),
            bodyHtml = $('body').html(),
            pageTitle = $('title').html();

        $('title').html('Presentie keuzedelen');
        $('body').html(printHtml).addClass('print');

        window.print();

        $('body').html(bodyHtml).removeClass('print');
        $('title').html(pageTitle);

    }
} )( jQuery );