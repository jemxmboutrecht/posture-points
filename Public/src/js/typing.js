speed       = 1;
fault       = 1;
simul       = true;

( function( $ ) {

    $(window).scroll(function() {
        var wScroll = $(window).scrollTop(),
            wHeight = $(window).outerHeight();

        $('.typing').each(function() {
            var elPosTop = $(this).position().top,
                elScrollTop = elPosTop - wScroll;

            if (elScrollTop + (wHeight * .12) < wHeight) {
                initiate_typing($(this));
            }
        });
    });

    var i = 0;

    $('.typing').each(function() {
       var elPosTop = $(this).position().top,
           wHeight = $(window).outerHeight();

       $(this).attr('data-typingid', i);

       if (elPosTop < wHeight) initiate_typing($(this));

       i++;
    });

    function initiate_typing(el) {

        var html = el.html(),
            typedEl = el.find('span.typed'),
            notTypedEl = el.find('span.not-typed'),
            typedArr = [],
            notTypedArr = html.split('');

        el.html('');

        if (typedEl.length < 1) {
            el.prepend('<span class="typed"></span>');
            typedEl = el.find('span.typed');
        } else {
            typedArr = typedEl.html().split('');
        }

        if (notTypedEl.length < 1) {
            el.append('<span class="not-typed"></span>');
            notTypedEl = el.find('span.not-typed');
        } else {
            notTypedArr = notTypedEl.html().split('');
        }

        if (el.hasClass('active-typ') === false) {
            el.addClass('active-tp');
            typing_loop(typedEl, notTypedEl, typedArr, notTypedArr);
        }
    }

    function typing_loop(typedEl, notTypedEl, typedArr, notTypedArr) {
        var faultFact = Math.random(),
            irrFact = 120 + ((faultFact * 10) * (fault * 20)),
            errFact = 1 - (fault * 0.1),
            nextLetter = notTypedArr.shift();

        if (faultFact > errFact && nextLetter !== ' ') {
            miss_type(typedEl, nextLetter, irrFact);
        }
        typedArr.push(nextLetter);

        if (notTypedArr.length > 0) {
            //console.log('hoi');
            notTypedEl.html( notTypedArr.join('') );
            typedEl.html( typedArr.join('') );
            setTimeout( function() {
                typing_loop(typedEl, notTypedEl, typedArr, notTypedArr)
            }, irrFact);
        }
    }

    function miss_type(typedEl, nextLetter, irrFact) {
        var kbObj = {
            1 : ['q','w','e','r','t','y','u','i','o','p'],
            2 : ['a','s','d','f','g','h','j','k','l'],
            3 : ['z','x','c','v','b','n','m']
        };

        $.each(kbObj, function( row ) {
            var iLetter = this.indexOf(nextLetter);

            if (iLetter > -1) {
                var nRow = generate_row_num(row),
                    niLetter = generate_letter_num(row, nRow, iLetter),
                    nLetter = kbObj[nRow][niLetter];

                typedEl.html(typedEl.html() + nLetter);

                setTimeout(function() {
                    correct_miss(typedEl, nextLetter, irrFact);
                }, irrFact);
            }
        });

    }

    function correct_miss(typedEl, nextLetter, irrFact) {
        console.log(irrFact+' :backspace');
        setTimeout((function() {
            typedEl.html(typedEl.html().slice(0, -1) + nextLetter)
        }), irrFact * 2000);
    }

    function generate_row_num(row) {
        if (row == 1) {
            var nLetterRow = random_int(1, 2),
                maxRow = 2,
                minRow = 1;
        } else if (row == 2) {
            var nLetterRow = random_int(1, 3),
                maxRow = 3,
                minRow = 1;
        } else {
            var nLetterRow = random_int(2, 3),
                maxRow = 3,
                minRow = 1;
        }

        if (nLetterRow > maxRow) nLetterRow = 3;
        else if (nLetterRow < minRow) nLetterRow = 1;

        return nLetterRow;
    }

    function generate_letter_num(row, nRow, iLetter) {
        var nLetterFact = random_int(-1, 1),
            rowCountObj = {1:9,2:8,3:6};

        if (nLetterFact < -1 ) nLetterFact = -1;
        else if (nLetterFact > 1) nLetterFact = 1;

        var niLetter = iLetter + nLetterFact;

        if (niLetter < 0) niLetter = 0;

        if (niLetter > rowCountObj[nRow]) niLetter = iLetter - nLetterFact;

        if (row == nRow && iLetter == niLetter) {
            if (Math.random() > .5) niLetter++;
            else niLetter--;
        }

        if (niLetter > rowCountObj[nRow]) return generate_letter_num(row, nRow, iLetter);
        else return niLetter;
    }

    function random_int(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
    }

} )( jQuery );