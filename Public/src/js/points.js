( function( $ ) {

    if ($('input[type="checkbox"]').length < 1 && $('div#log-container.student').length < 1) get_students('');

    $('form input#firstname').keypress(function(key) {
        if (key.keyCode == 13) {
            key.preventDefault();
            $(this).parents('form').submit(function(form) {
                form.preventDefault();
                return false;
            });
        }
    });

    $('form input:not(#firstname)').focus(function() {
        $(this).parents('form').unbind();
    });

    $('form input#firstname').keyup(function(e) {
        if(e.keyCode == 13) {
            pin_student();
        }
        var admin = ($(this).hasClass('admin')?true:false);
        get_students( $(this).val(), admin );
    });

    $('div#student-overview').on('click', 'button.pointbtn', function(e) {
        var type = $(this).data('id'),
            point = $(this).data('point'),
            userid = $(this).parents('div.buttons').data('id'),
            name = $(this).parents('div.buttons').data('name'),
            points = $(this).parents('div.buttons').data('points'),
            max = $(this).parents('div.buttons').data('max'),
            vars = {type: type, point: point, userid: userid, points: points, max: max},
            callback = (function (data) {
                bflb( name, data );
            });

        bfajax( 'studentpoints', 'point_form', vars, callback);
    });

    $('div.logblock.student').click(function() {
        $(this).find('div.icon').toggleClass('active');
    });

    $('div#log-container:not(.student) div.logblock').click(function() {
        var time = $(this).data('time'),
            callback = (function(data) {
                bflb('Punt aanpassen', data, 'large', (function(e){close_bflb()}));
            });

        bfajax('studentpoints', 'edit_form', {time:time}, callback);
    });

    $('div.lb-container').on('change', 'select#type', function() {
        var types = $(this).parents('form#form-edit-log').data('points'),
            types = JSON.parse(types.replace(/'/g, '"')),
            typeId = $(this).val();

        $('input#point').val(types[typeId]).attr('value', types[typeId]);
    });

    $('div.lb-container').on('click', 'button#delete-point', function() {
        var time = $(this).data('time'),
            point = $(this).data('point'),
            points = $(this).data('points'),
            userId = $(this).data('id'),
            conf = (function(d) {
                if (d) {
                    var callback = (function (data) {
                        if (data == '1') {
                            reload();
                        } else {
                            bflb('Oeps!', 'Er is iets fout gegaan', null, 'reload');
                        }
                    });
                    bfajax('Studentpoints', 'delete_point', {time: time, point: point, points: points, userId: userId}, callback);
                } else {
                    close_bflb();
                }
            });
        bfconf('Punt verwijderen?', 'Weet je zeker dat je dit wilt verwijderen?', conf);
    });

    function get_students( input, admin ) {
        var callback = (function(data) {
            $('div#student-overview').html(data);
            get_count();
        });

        if (admin && input.length < 1) input = null;

        bfajax('studentpoints', 'get_students', {input: input}, callback);
    }

    function get_count() {
        var studentArr = [];

        $('div#student-overview div.pointblock div.buttons').each(function() {
            studentArr.push($(this).data('id'));
        });

        var callback = (function(data) {
            var countArr = JSON.parse(data);

            countArr.each(function() {

            });
        });

        bfajax('studentpoints', 'get_count', {userIds: studentArr}, callback);
    }

    function pin_student()
    {
        if ($('div#student-overview').hasClass('set')) {
            var blockel = $('div#student-overview div.pointblock:first-of-type'),
                userid = blockel.find('div.buttons').data('id'),
                blockHTML = blockel.clone();

            blockel.remove();
            $('div#pinned').prepend(blockHTML);

            var ids = $('input#userid[type="hidden"]').val(),
                idArr = (ids.length < 1 ? [] : ids.split(','));

            idArr.push(userid);

            $('input#userid[type="hidden"]').attr('value', idArr.join(',')).val(idArr.join(','));
        }
    }

} )( jQuery );