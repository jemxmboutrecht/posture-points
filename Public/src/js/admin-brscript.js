( function( $ ) {
    $('ul#option-overview-list li i.dropdown-icon').click(function() {
        var categoryEl = $(this).parent('li[data-key="category"]'),
            newHeight = (categoryEl.children('ul.sub').children('li').length * 3.2 + 3.2);

        if (categoryEl.hasClass('open') === false) {
            categoryEl.animate({height: newHeight + 'rem'}).addClass('open');
        } else {
            categoryEl.animate({height: '3.2rem'}).removeClass('open');
        }
    });
} )( jQuery );