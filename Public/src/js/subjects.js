( function( $ ) {

    $('div.sublink').click(function() {
        var buttonEl = $(this),
            studentEl = $(this).parents('tr').find('strong.student'),
            userId = studentEl.data('userid'),
            student = studentEl.html(),
            subjectTitle = $(this).attr('title'),
            identifier = $(this).parent('td.subcol').data('subject');

        if ($(this).hasClass('subbed')) {
            var action = 'uitschrijven';
        } else {
            var action = 'inschrijven';
        }

        /*var message = 'Weet je zeker dat je <strong>'+student+'</strong> wilt '+action+' voor <strong>'+subjectTitle+'</strong>?',
            callback = (function(conf){subEdit(conf, action, identifier, userId, buttonEl)});

        bfconf(ucfirst(action), message, callback);*/
        subEdit(true, action, identifier, userId, buttonEl);
    });

    function subEdit(conf, action, identifier, userid, buttonEl) {
        if (conf) {
            var func = (action == 'inschrijven' ? 'sub' : 'unsub')+'_student',
                callback = (function(resp) {
                    console.log(resp);
                    if (resp == 0) alert('Er is iets fout gegaan, probeer het later opnieuw!');
                    else buttonEl.toggleClass('subbed')
                });
            bfajax('subject', func, {'identifier':identifier,'userid':userid}, callback);
        }
        //close_bflb();
    }
} )( jQuery );