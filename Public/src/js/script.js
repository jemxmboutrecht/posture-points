( function( $ ) {

    $('div.background > svg, div.background > img').addClass('active');
    set_menu_classes( $(window).outerWidth() );
    set_table_labels();

    $(window).on('resize', function() {
        var viewWidth = $(this).outerWidth();
        console.log($(window).outerWidth());
        console.log($(window).outerHeight());

        set_menu_classes( viewWidth );
    });

    $(window).on('keydown', function(e) {
        var key = e.key;

        if (key == 'Escape') {
            if ($('div.lb-container.active').length > 0) {
                close_bflb();
            }
        }
    });

    if ($('div.wysiwyg').length > 0 ) {
        var wysiwyg = $('div.wysiwyg').get(0);
        var quill = new Quill(wysiwyg, {
            theme: 'snow',
            modules: {toolbar: true}
        });
    }

    if ($('div.wysiwyg').length > 0) {
        var wysiwyg = $('div.wysiwyg .ql-editor'),
            inputEl = $('#'+$('div.wysiwyg').attr('for')),
            formEl = $('div.wysiwyg').parents('form');

        formEl.on('submit', function(e) {
            if ($(this).hasClass('submitted') === false) {
                e.preventDefault();

                var elId = $('div.wysiwyg').attr('for'),
                    inputEl = $('#'+elId);

                inputEl.attr('value', wysiwyg.html());

                formEl.addClass('submitted');
                formEl.submit();
            }
        });
    }

    if ($('.datepicker').length > 0) {
        var valDate = $('input.datepicker').attr('value');
        if (typeof valDate !== 'undefined') {
            dateObj = new Date(valDate);
        } else {
            dateObj = new Date();
        }
    } else {
        dateObj = new Date();
    }

    if ($('.datepicker').length > 0) {
        var datepicker = pickmeup('.datepicker', {
            date: dateObj,
            hide_on_select: true,
            format: 'a d b'
        });
    }

    $('select.linkselect').on('change', function() {
        var val = $(this).val(),
            link = $(this).data('link');

        if (link.length < 0 ) {
            console.log('data-link attribute must be set on select element.');
            return;
        }

        window.location.href = link+val;
    });

    $('div.user-menu-container>a.user-title').click(function() {
        $(this).toggleClass('active');
        $('ul.user-menu').toggleClass('active');
    });

    $('nav div.menu-container div.menu-icon').click(function() {
        $(this).children('div.hamburger').toggleClass('is-active');
        $(this).parents('div.menu-container').toggleClass('open');
        $('body main').toggleClass('open-menu');
    });

    $('div.lb-ct-container>div.close-lb').click(function() {
        close_bflb();
    });

    $('body').on('submit', 'div.ct>form', function(e) {
        e.preventDefault();
        var inputArr = gen_input_array($(this)),
            obj = ($(this).attr('bfa-obj')?$(this).attr('bfa-obj'):false),
            func = ($(this).attr('bfa-func')?$(this).attr('bfa-func'):false),
            callback = ($(this).attr('bfa-callback')?$(this).attr('bfa-callback'):'reload');

        bfajax(obj, func, inputArr, callback);
    });

    $('table.overview td.editcol button').click(function() {
        var func = $(this).data('func'),
            title = $(this).parents('table').attr('id'),
            vars = {};

        if ( func == 'edit' || func == 'delete' || func == 'activate' || func == 'uninstall' ) {
            var idLabel = title + '_id',
                rowEl = $(this).parents('tr')
                niceLabel = (typeof rowEl.data('label') != 'undefined' ? rowEl.data('label') : 'id'),
                niceTitle = (typeof rowEl.data('title') != 'undefined' ? rowEl.data('title') : title);

            vars[idLabel] = rowEl.data('id');
        } else {
            var niceTitle = title;
        }

        if ( func == 'edit' ) var niceFunc = 'wijzigen';
        else if ( func == 'delete' ) var niceFunc = 'verwijderen';
        else var niceFunc = func;


        if ( func == 'install' || func == 'activate' ) {
            vars[func+'Var'] = $(this).parents('tr').children('td[data-id="'+$(this).data(func)+'"]').children('p').html();
        }

        if ( func == 'delete' || func == 'activate' || func == 'uninstall' ) {
            var check = ( function(conf) {
                    if (!conf) {
                        close_bflb();
                        return false;
                    } else {
                        bfajax( title, func+'_'+title, vars, 'reload');
                    }
                });
            if (func == 'delete') {
                var message = 'Weet je zeker dat je '+niceTitle+' '+niceLabel+' wilt '+niceFunc+'?';
            } else {
                var message = 'Are you sure you wanna '+func+' the '+title+' with id: '+vars[idLabel]+'?';
            }
            bfconf(niceTitle+' '+niceFunc+'?', message, check);

            //bfajax( title, func+'_'+title, vars, true);
        } else {
            if ( func == 'edit' || func == 'add' || func == 'install' || func == 'activate' || func == 'uninstall' ) {
                callback = (function (data) {
                    bflb( niceTitle+' '+niceFunc, data );
                });
            }

            bfajax( title, func+'_'+title, vars, callback);
        }
    });

    $('table.overview tr[data-url]').click(function(e) {
        if ( !$(e.target).is('button[data-func]') && $(e.target).parents('button[data-func]').length == 0 ) {
            e.preventDefault();

            var rowEl = $(e.target).parents('tr');

            if (typeof $(e.target).parents('tr[data-url]').data('getlink') !== 'undefined') {
                var url = rowEl.data('url'),
                    get = rowEl.data('getlink'),
                    id = rowEl.data('id'),
                    linkUrl = url+'/?'+get+'='+id;
            } else {
                var linkUrl = rowEl.data('url');
            }

            window.location.href = linkUrl;
        }
    });

    $('button.planningbtn').click(function() {

        var func = $(this).data('func');

        var vars = {'id': $(this).data('id')};

        if (func == 'check' || func == 'uncheck') {
            vars['userid'] = $(this).data('userid');
        }

        if (func == 'delete') {
            var name = $(this).parent('.editcol').data('name'),
                callback = (function(data){
                    reload();
                }),
                delFunc = (function(data) {
                    if (data) bfajax('subject', 'delete_planning', vars, callback);
                });
        }

        if (func == 'delete') {
            bfconf('Delete', 'Weet je zeker dat je <strong>'+name+'</strong> wilt verwijderen uit de planning?', delFunc);
        } else {
            var callback = (function(data) {
                    bflb(title, data);
                });
            bfajax('subject', func + '_planning', vars, 'reload');
        }
    });

    $('div.open-planning').click(function() {
       var id = $(this).data('id'),
           callback = (function(data) {
               if (data) {
                   var planning = JSON.parse(data);
                   bflb(planning.name, planning.description,'big');
               }
           });
       bfajax('subject', 'open_planning', {id: id}, callback);
    });

} )( jQuery );


function set_menu_classes( viewWidth ) {
    /*if (viewWidth < 992) {
        $('nav.navbar').addClass('admin-navbar');
    } else {
        $('nav.navbar.admin-navbar').removeClass('admin-navbar');
    }*/
}

function set_table_labels() {
    /*$('table').each(function() {
        $(this).find('th').each(function() {
            var id = $(this).data('id'),
                val = $(this).html();

            if ( val.length > 0 ) {
                $('td[data-id="'+id+'"]').prepend('<label class="col-lbl">'+val+'</label>');
            }
        });
    });*/
}

function bfajax( object, functionName, functionVars, returnResp ) {
    var object = ( !object ? null : object ),
        functionName = ( !functionName ? null : functionName ),
        functionVars = ( !functionVars ? null : functionVars ),
        returnResp = ( !returnResp ? true : returnResp ),
        baseAjaxUrl = 'http://localhost/posture-points/ajax/';

    if ( object !== null ) {
        baseAjaxUrl += object + '/';
    }

    if ( functionName !== null ) {
        baseAjaxUrl += functionName + '/';
    }

    if ( functionVars !== null ) {
        var count = Object.keys(functionVars).length;
            i = 1;
        $.each(functionVars, function( key, val ) {
            baseAjaxUrl += key + '=' + val;
            if (i < count) baseAjaxUrl += '&';
            i++;
        });
    }
    console.log(baseAjaxUrl);
    $.post({
        url: baseAjaxUrl,
    }).done( function( response ) {
            if (response == '404') {
                alert('U heeft geen toegang tot deze functie.');
                return;
            }
            if ( returnResp == true ) {
                return response;
            } else if ( typeof returnResp == 'function' ) {
                returnResp( response );
            } else if ( typeof returnResp == 'string' ) {
                if ( returnResp == 'log' ) {
                    console.log( response );
                } else if (check_if_func(returnResp) !== false) {
                    var fn = eval(returnResp);
                    fn(response);
                } else {
                    $( returnResp ).html( response );
                }
            } else {
                return response;
            }
        }
    );

    return false;
}

function check_if_func(returnResp) {
    if (typeof eval(returnResp) === "function" ) return true;
    else return false;
}

function gen_input_array( formEl ) {
    var inputArr = {};

    formEl.find( 'input:not([type="radio"],[type="submit"]), input[type="radio"]:checked, textarea, select' ).each( function() {

        var inputName = $( this ).attr( 'name' ),
            inputVal = $( this ).val(),
            subStrPos = inputName.indexOf( '[]' );

        if ( subStrPos > -1 ) {
            var multiInputName = inputName.substring( 0, subStrPos );

            if ( typeof inputArr[ multiInputName ] === 'undefined' ) {
                inputArr[ multiInputName ] = [ inputVal ];
            } else {
                inputArr[ multiInputName ].push( inputVal );
            }

        } else {
            inputArr[ inputName ] = inputVal;
        }


    });
    return inputArr;
}

function bflb( title, content, size, onClose ) {
    var title = (!title ? null: title),
        size = (!size ? 'medium' : size),
        onClose = (!onClose ? 'not-active' : onClose),
        lbContainer = $( 'div.lb-container' ),
        ctContainer = lbContainer.children( 'div.lb-ct-container' ),
        lbContent = ctContainer.children( 'div.lb-ct' ),
        lbTitle = lbContent.children( 'h2.lb-title' ),
        ctElement = lbContent.children( 'div.ct' );

    if ( content.length > 0 ) {

        ctElement.html( content );

        if ( title !== null ) lbTitle.html( title );

        if ( lbContainer.hasClass( 'active' ) === false ) {
            lbContainer.css( 'display', 'block' );
            ctContainer.css( 'display', 'block' );

            if ( size !== null ) {
                switch( size ) {
                    case 'big':
                    case 'container':
                        ctContainer.addClass( 'container' );
                        break;
                    case 'medium':
                        ctContainer.addClass( 'medium' );
                        break;
                    case 'small':
                        ctContainer.addClass( 'small' );
                        break;
                }
            }

            setTimeout( function() {
                lbContainer.addClass( 'active' ).attr('data-onclose', onClose);
                ctContainer.addClass( 'active' );
            }, 10 );
        }

    } else {
        console.log( 'No content given for lightbox.' );
    }

}

function bfconf( title, message, callback, type ) {
    var title = (!title ? false : title),
        message = (!message ? 'Do you confirm?' : message),
        callback = (!callback ? (function(s){console.log(s)}) : callback),
        type = (!type ? 0 : type);

    switch (type) {
        case 0 :
            choice = {'Ja': true,'Nee': false};
            break;
        case 1 :
            choice = {'Nee': false};
            break;
        case 2 :
            choice = {'Ja': true};
            break;
    }

    var confHtml = message+'<br>';

    $.each(choice, function( key, val ) {
        confHtml += '<button class="bfconf-button" data-val="'+val+'">'+key+'</button>';
    });

    bflb(title, confHtml);

    $('button.bfconf-button').click(function(){
        callback($(this).data('val'));
    });

}

function close_bflb() {
    var lbContainer = $( 'div.lb-container' ),
        onClose = lbContainer.data('onclose'),
        ctContainer = lbContainer.children( 'div.lb-ct-container' ),
        lbContent = ctContainer.children( 'div.lb-ct' ),
        lbTitle = lbContent.children( 'h2.lb-title' ),
        ctElement = lbContent.children( 'div.ct' );
    console.log(onClose);
    if ( lbContainer.hasClass( 'active' ) === true && ctContainer.hasClass( 'active' ) === true ) {
        ctContainer.removeClass( 'active' );

        setTimeout( function() {
            lbContainer.removeClass( 'active' );
        }, 250 );

        setTimeout( function() {
            lbContainer.removeClass( 'active' ).removeAttr( 'style' );
            ctContainer.attr( 'class', 'lb-ct-container' ).removeAttr( 'style' );
            lbTitle.html( '' );
            ctElement.html( '' );
        }, 700 );
    }
}

function reload() {
    location.reload();
}

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}