(function($) {

    var sumArr = JSON.parse($('div#points-arr').html()),
        colorArr = JSON.parse($('div#color-arr').html()),
        borderArr = JSON.parse($('div#border-arr').html());

    var ctx = document.getElementById('point-chart');
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                label: '# of Votes',
                data: sumArr,
                backgroundColor: colorArr,
                borderColor: borderArr,
                borderWidth: 2
            }]
        }
    });

}) (jQuery);