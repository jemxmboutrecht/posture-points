<html>
<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script
        src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
        crossorigin="anonymous"></script>

    <style>
        body * {
            box-sizing: border-box;
        }
        ul.top {
            display: block;
            padding: 0;
            background-color: transparent;
            border: 1px solid #ccc;
            overflow: hidden;
        }
        ul {
            display: none;
            position: relative;
            float: left;
            width: 100%;
            padding: 0 0 0 15px;
            list-style-type: none;
        }
        ul.visible {
            display: block;
        }
        ul li {
            position: relative;
            float: left;

            padding: 10px 0 0 20px;
        }
        ul li i {
            float: left;
            width: 25px;
            height: 30px;
            margin-right: 10px;
        }
        li.file {
            width: calc( 100% + 25px );
            margin-left: -15px;
            font-weight: bold;
            background-color: #fff;
        }
        li.directory {
            width: calc( 100% + 25px );
            padding-bottom: 0;
            background-color: rgba(0,0,0,0.05);
            border-bottom: 1px solid #ccc;
            cursor: pointer;
        }
        li.directory > ul {
            margin-left: -20px;
            border: 1px solid #ccc;
        }


    </style>
</head>
<body>
<?php

    $exceptions = array( '.', '..', '.htaccess', '.git', '.idea', 'file_list.php' );

    echo generate_list_from_dir('', $exceptions);

    function generate_list_from_dir( $parent, $exceptions ) {

        $listStr = '';

        if ( empty($parent) ) $listStr .= '<ul class="top">';
        else $listStr .= '<ul>';

        $fileDir = sort_dir( scandir(__DIR__ . '\\' . $parent) );

        foreach ( $fileDir as $sub ){
            if ( !in_array($sub, $exceptions) ) {
                if (strpos($sub, '.') != false) {
                    $listStr .= generate_file_item( $sub );//'<li class="file"><i class="fa fa-file"></i>' . $sub . '</li>';
                } else {
                    $listStr .= '<li class="directory"><i class="fa fa-folder"></i>' . $sub;
                    $listStr .= generate_list_from_dir($parent . '\\' . $sub, $exceptions);
                    $listStr .= '</li>';
                }
            }
        }
        $listStr .= '</ul>';

        return $listStr;
    }

    function generate_file_item( $sub ) {
        $iconArr = array(
            'php'   => 'fab fa-php',
            'phtml' => 'fas fa-code',
            'ini'   => 'fas fa-code',
            'js'    => 'fab fa-js-square',
            'css'   => 'fab fa-css3',
            'scss'  => 'fab fa-sass',
            'jpg'   => 'far fa-image',
            'png'   => 'far fa-image',
            'gif'   => 'far fa-image',
            'jpeg'  => 'far fa-image',
            'svg'   => 'fas fa-bezier-curve',
            'ttf'   => 'fas fa-font',
            'eot'   => 'fas fa-font',
            'woff'  => 'fas fa-font',
            'woff2'  => 'fas fa-font'
        );
        $fileArr = explode('.', $sub);
        $fileExt = end($fileArr);
        if ( isset($iconArr[$fileExt]) ) $fileIco = $iconArr[$fileExt];
        else $fileIco = 'fa fa-file';

        return '<li class="file '.$fileExt.'"><i class="'.$fileIco.'"></i>'.$sub.'</li>';

    }

    function sort_dir( $dirArr ) {
        $dirs = array();
        $files = array();
        foreach ($dirArr as $sub) {
            if (strpos($sub, '.') != false) {
                $files[] = $sub;
            } else {
                $dirs[] = $sub;
            }
        }
        return array_merge($dirs, $files);
    }
?>
<script>
    $('li.directory').click(function(e) {
        e.stopPropagation();
        console.log($(e.target));
        console.log($(this));
        if ($(this).hasClass('opened')) {
            $(e.target).removeClass('opened').children('ul.visible').removeClass('visible');
        } else {
            $(e.target).addClass('opened').children('ul:not(.visible)').addClass('visible');
        }
    });
</script>
</body>
</html>
