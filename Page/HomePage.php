<?php

namespace page;

class HomePage extends \Page\PageAbstract {

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );
        $this->pageObj->routeObj->go_to('dashboard');
    }
}