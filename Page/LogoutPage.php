<?php

namespace page;

class LogoutPage extends \Page\PageAbstract {

    public function __construct( $pageObj )
    {
        \base\controllers\ApplicationController::perlog();
        $this->set_pageObj( $pageObj );

        session_destroy();

        \base\controllers\ApplicationController::go_to( 'home' );

        \base\controllers\ApplicationController::perlog();
    }
}