<?php

namespace page;

abstract class PageAbstract {

    public $pageObj;

    public $backButton;

    public function set_pageObj( object $pageObj )
    {
        $this->pageObj = $pageObj;
    }

    public function set_backButton( string $url, string $text )
    {
        $this->backButton = '<button class="col1 inv backbutton">';
        $this->backButton .= '<a href="'.$url.'"><i class="fas fa-arrow-left"></i>'.$text.'</a>';
        $this->backButton .= '</button>';
    }

    public function get_backButton()
    {
        return $this->backButton;
    }
}