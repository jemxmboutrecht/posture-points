<?php

namespace page;

class PointlogPage extends \Page\PageAbstract {

    private $type;

    private $identifier;

    private $user;

    private $logSql;

    public $log;

    public $student = false;

    private $userId;

    private $userLevel;

    public $pointArr;

    public $pointSum;

    public $pointChart;

    public function __construct( $pageObj )
    {
        $this->set_pageObj($pageObj);

        \base\controllers\PageController::set_dependencies('footer', 'points', 'css', null, 'pointstyle');
        \base\controllers\PageController::set_dependencies('footer', 'points', 'js', null, 'points', 'jquery');

        $this->type = $this->pageObj->routeObj->get_pageVars('type');
        $this->identifier = $this->pageObj->routeObj->get_pageVars('identifier');

        $this->user = new \base\controllers\UserController();
        $this->user->set_user_by_session();
        $this->userLevel = $this->user->get_level();

        if ($this->identifier && $this->user->get_level() <= 2) {
            $this->user->get_user_by(array('user_id' => $this->identifier), true);
        }

        $this->userId = $this->user->get_userId();

        $this->logSql = 'SELECT 
                                pl.logtime AS time,
                                pl.point AS point,
                                pl.description AS description,
                                pt.name AS name,
                                pt.icon AS icon,
                                pt.type AS type,
                                sp.points AS points, 
                                sp.maximum AS max,
                                AES_DECRYPT(us.firstname, "'.AES.'") as firstname,
                                AES_DECRYPT(us.prefix, "'.AES.'") as prefix,
                                AES_DECRYPT(us.lastname, "'.AES.'") as lastname,
                                AES_DECRYPT(ut.username, "'.AES.'") as lettercode
                            FROM point_log AS pl
                            LEFT JOIN point_type AS pt ON pl.point_type = pt.type_id 
                            LEFT JOIN user AS us ON pl.student_id = us.user_id
                            LEFT JOIN user AS ut ON pl.teacher_id = ut.user_id
                            LEFT JOIN student_points AS sp ON pl.student_id = sp.user_id
                            WHERE [wherestr] ORDER BY logtime DESC LIMIT 50';

        if ($this->type == 'student' || $this->userLevel == 5) {
            \base\controllers\PageController::set_dependencies('footer', 'Chart.min', 'css', 'lib', 'chartstyle');
            \base\controllers\PageController::set_dependencies('footer', 'Chart.min', 'js', 'lib', 'chart', 'jquery');
            \base\controllers\PageController::set_dependencies('footer', 'pointchart', 'js', null, 'pointchart', 'chart');
            $this->student = true;

            $this->pageObj->routeObj->set_altTitle($this->user->get_username().' - '.$this->user->get_nicename());

            $this->get_spec_points();
            $this->get_student_log();
            $this->get_point_chart();
        } elseif ($this->type == 'log') {
            $this->get_log_detail();
        } else {
            $this->get_log();
        }
    }

    private function get_log()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query(str_replace('[wherestr]', 'teacher_id = :userId', $this->logSql));
        $db->bind(':userId', $this->user->get_userId());

        if ( $db->resultset() ) {
            $this->generate_log( $db->resultset() );
        }
    }

    private function get_spec_points()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT
	                            pl.point_type AS pointtype,
                                COUNT(pl.point_type) AS count,
                                pt.point AS point,
                                pt.name AS name,
                                pt.icon AS icon,
                                pt.type AS type
                            FROM point_log AS pl
                            LEFT JOIN point_type AS pt ON pl.point_type = pt.type_id
                            WHERE student_id = :userId
                            GROUP BY point_type
                            ORDER BY count DESC');
        $db->bind(':userId', $this->userId);

        if ($points = $db->resultset()) {
            $resultArr = array();
            foreach ($points as $point) {
                $resultArr[$point['pointtype']]['type'] = $point['type'];
                $resultArr[$point['pointtype']]['icon'] = $point['icon'];
                $resultArr[$point['pointtype']]['count'] = $point['count'];
                $resultArr[$point['pointtype']]['point'] = $point['point'];
                $resultArr[$point['pointtype']]['sum'] = $point['count'] * $point['point'];
                $resultArr[$point['pointtype']]['name'] = $point['name'];
            }
            $this->pointSum = $resultArr;
        }

    }

    private function get_student_log()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query(str_replace('[wherestr]', 'pl.student_id = :userId', $this->logSql));
        $db->bind(':userId', $this->userId);

        if ( $db->resultset() ) {
            $this->pointArr = array($db->resultset()[0]['points'], $db->resultset()[0]['max']);
            $this->generate_log( $db->resultset() );
        } else {
            $db->query('SELECT points, maximum FROM student_points WHERE user_id = :userId');
            $db->bind(':userId', $this->user->get_userId());
            if ( $result = $db->resultset() ) {
                $this->pointArr = array($db->resultset()[0]['points'], $db->resultset()[0]['maximum']);
            } else {
                $this->pointArr = '<em>Er zijn nog geen punten voor je ingesteld, neem contact op met de betreffende docent.</em>';
            }
        }
    }

    private function get_point_chart()
    {
        $this->pointChart = $this->pageObj->get_part('pointchart', array($this->pointSum, $this->pointArr), true);
    }

    private function get_log_detail()
    {

    }

    private function generate_log( array $logArr )
    {
        $resultStr = '';
        if ($this->user->get_level() <= 2) $teacher = true;
        else $teacher = false;

        $currentDay = 0;
        $weekArr = array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
        $monthArr = array('januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'october', 'november', 'december');

        foreach ( $logArr as $log ) {
            $log['teacher'] = $teacher;
            $day = date('j', strtotime($log['time']));
            $weekday = date('w', strtotime($log['time']));
            $month = date('w', strtotime($log['time']));
            if ($currentDay != $day) {
                $resultStr .= '<h2>'.ucfirst($weekArr[$weekday]).' '.$day.' '.$monthArr[$month - 1].'</h2>';
                $currentDay = $day;
            }
            $resultStr .= $this->pageObj->get_part('logblock', $log, true);
        }

        $this->log = $resultStr;
    }

}