<?php

namespace page;

class DashboardPage extends \Page\PageAbstract {

    private $userId;
    private $level;

    private $today;

    public $view;

    public function __construct( $pageObj )
    {
        $this->set_pageObj( $pageObj );

        \base\controllers\PageController::set_dependencies('footer', 'points', 'css', null, 'pointstyle');
        \base\controllers\PageController::set_dependencies('footer', 'points', 'js', null, 'points', 'jquery');

        if (!$this->pageObj->userObj) $this->pageObj->routeObj->go_to('login');

        $this->userId = $this->pageObj->userObj->get_userId();
        $this->level = $this->pageObj->userObj->get_level();

        $this->today = date('N', strtotime('now'));

        if ($this->level <= 2) $this->admin_start();
        elseif ($this->level == 5) $pageObj->routeObj->go_to('pointlog');
    }

    public function admin_start()
    {
        $vars = array();

        $form = new \base\controllers\FormController('search-student');
        $form->set_input('text', 'firstname', array( 'label'=> '', 'attributes' => array('placeholder'=>'Voornaam','autocomplete'=>'off') ) );

        $vars['form'] = $form->generate_form();

        $this->view = $this->pageObj->get_part('adminstart', $vars, true);
    }


}