<?php

namespace page;

class ProfilePage extends \Page\PageAbstract
{

    public $profileForm;

    public $formMessage;

    public function __construct($pageObj)
    {
        $this->set_pageObj($pageObj);
        $this->pageObj->routeObj->set_altTitle('Profiel');

        $this->generate_profile_form();

        if (isset($this->profileForm) && $this->profileForm->get_formValues()) {
            $this->edit_user();
        }
    }

    private function generate_profile_form()
    {
        $formObj = new \base\controllers\FormController( 'edit-user', 'post' );
        $formObj->set_input( 'password', 'newpassword', array('label' => 'Nieuw wachtwoord', 'required' => true) );
        $formObj->set_input( 'password', 'curpassword', array('label' => 'Huidig wachtwoord ', 'required' => true) );
        $formObj->set_input( 'submit', 'Wijzigen' );
        $this->profileForm = $formObj;
    }

    private function edit_user()
    {
        $formVals = $this->profileForm->get_formValues();

        if ($this->pageObj->userObj->verify_password($formVals['curpassword'], null, true)){
            $query = 'UPDATE user SET
                                [newpasssql]
                               WHERE user_id = :userId';

            $newPass = $this->pageObj->userObj->set_new_user_password($formVals['newpassword'], true);
            $sqlStr = 'password = AES_ENCRYPT(:password, "'.AES.'")';
            $query = str_replace('[newpasssql]', $sqlStr, $query);

            $db = \base\controllers\ApplicationController::get_db();
            $db->query($query);

            $db->bind(':userId', $this->pageObj->userObj->get_userId());

            if (isset($formVals['newpassword'])) $db->bind(':password', $newPass);

            if ($db->execute()) {
                $this->formMessage = array('class' => 'success', 'message' => 'Gelukt! De wijzigingen zijn opgeslagen.');
            } else {
                $this->formMessage = array('class' => 'warning', 'message' => 'Er is iets misgegaan. Probeer het later nog eens of neem contact op met de administrator.');
            }
        } else {
            $this->formMessage = array('class' => 'danger', 'message' => 'Huidig wachtwoord is verkeerd. Wijzigingen zijn niet opgeslagen.<br>Probeer het opnieuw.');
        }
    }
}