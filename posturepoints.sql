-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 08, 2020 at 11:29 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posturepoints`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
CREATE TABLE IF NOT EXISTS `class` (
  `class_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `nicename` varchar(10) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(5) DEFAULT NULL,
  PRIMARY KEY (`class_id`),
  KEY `mentor_id` (`user_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_student`
--

DROP TABLE IF EXISTS `class_student`;
CREATE TABLE IF NOT EXISTS `class_student` (
  `class_id` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`class_id`,`user_id`),
  KEY `class_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_teacher`
--

DROP TABLE IF EXISTS `class_teacher`;
CREATE TABLE IF NOT EXISTS `class_teacher` (
  `class_id` int(5) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`class_id`,`user_id`),
  KEY `teacher_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `course_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `nicename` varchar(10) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_title`) VALUES
(1, 'Default'),
(2, 'admin-menu');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE IF NOT EXISTS `menu_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT '0',
  `item_title` varchar(50) NOT NULL DEFAULT '0',
  `item_url` varchar(255) NOT NULL DEFAULT '0',
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`item_id`, `menu_id`, `parent_id`, `item_title`, `item_url`, `level`) VALUES
(2, 1, 0, 'Dashboard', 'dashboard', 5),
(4, 2, 0, 'Dashboard', 'dashboard', 0),
(5, 2, 0, 'Users', 'users', 0),
(10, 2, 5, 'Students', 'students', 0),
(12, 2, 0, 'Klassen en opleidingen', 'class', 0),
(19, 2, 5, 'Student import', 'studentimport', 0),
(20, 2, 0, 'Set points', 'setpoints', 0),
(21, 1, 0, 'Punten logboek', 'pointlog', 5),
(22, 2, 12, 'Docenten koppelen', 'teacherlink', 0);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
CREATE TABLE IF NOT EXISTS `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `edited` datetime NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`page_id`),
  UNIQUE KEY `page_url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page_content`
--

DROP TABLE IF EXISTS `page_content`;
CREATE TABLE IF NOT EXISTS `page_content` (
  `content_id` int(6) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `content_type` int(11) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `page_id` (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `point_log`
--

DROP TABLE IF EXISTS `point_log`;
CREATE TABLE IF NOT EXISTS `point_log` (
  `logtime` timestamp NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `point_type` int(2) NOT NULL,
  `point` int(1) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`logtime`),
  KEY `teacher_id` (`teacher_id`),
  KEY `student_id` (`student_id`),
  KEY `point_type` (`point_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `point_type`
--

DROP TABLE IF EXISTS `point_type`;
CREATE TABLE IF NOT EXISTS `point_type` (
  `type_id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `point` int(1) NOT NULL,
  `icon` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_points`
--

DROP TABLE IF EXISTS `student_points`;
CREATE TABLE IF NOT EXISTS `student_points` (
  `user_id` int(11) NOT NULL,
  `points` int(2) NOT NULL,
  `maximum` int(2) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `system_options`
--

DROP TABLE IF EXISTS `system_options`;
CREATE TABLE IF NOT EXISTS `system_options` (
  `option_key` varchar(20) NOT NULL,
  `option_value` varchar(255) NOT NULL,
  PRIMARY KEY (`option_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varbinary(66) NOT NULL,
  `email` varbinary(271) NOT NULL,
  `password` varbinary(271) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `firstname` varbinary(116) NOT NULL,
  `prefix` varbinary(66) NOT NULL,
  `lastname` varbinary(271) NOT NULL,
  `role_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username, email` (`username`,`email`),
  KEY `role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `hash`, `firstname`, `prefix`, `lastname`, `role_id`) VALUES
(2, 0xe96950bb30af856d05e69d142c1ceb93, 0xbbbeaf15e6f42ccaf217ac58e49aee48c389ba541399313ec881e40ff096aa6c, 0xa10a45d413be17700568a1c8bdfc80f3ee18b5f306f149558a6ed645c9b2ed33d8a5f8e282ec141273eb07b71f952ca70cf5633b30a85f4203b5e489cf662e2e, '?hnx^nTxqtL+!*Y_HT&SxM8OqRP37at4', 0x8afbf3e895e9c85a740d6916f9e64574, '', 0x77601e96fae3f80977949aff5ef54fca, 1),
(153, 0x2cc6f12a75cd58ba49aab5574bff94e6, 0xbeb7769768cef2cbd5c654d1eb7289c97d2943fc9412f4cd779a0fcf39138b90, 0x4c622efb67608227e80def62d165775556e14ae69d1cf77290a38759dea9ebe0657db216236f60c42f8d43cc0ddc58d303549ec4c974ae2483ef10743960eafa, '1RBkEgRj5Y8?-Ff&nJe*Qar2sFI!HBSH', 0x550c8662cca9a4e666e9b099876c1da4, 0xec0a953cdbbbd6f8b614076ba8cb97c5, 0x625c58e0fdf2607da3b866b4b7e69a6c, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
CREATE TABLE IF NOT EXISTS `user_meta` (
  `user_id` int(11) NOT NULL,
  `meta_key` varchar(32) NOT NULL,
  `meta_value` varbinary(271) NOT NULL,
  PRIMARY KEY (`user_id`,`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `role_id` int(2) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `level` int(2) NOT NULL DEFAULT '3',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `title`, `description`, `level`) VALUES
(1, 'superadmin', '', 0),
(2, 'admin', '', 1),
(3, 'moderator', '', 2),
(4, 'user', '', 3),
(5, 'student', '', 5),
(6, 'teacher', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `visit_log`
--

DROP TABLE IF EXISTS `visit_log`;
CREATE TABLE IF NOT EXISTS `visit_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `page` varchar(100) NOT NULL,
  `variables` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3712 DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `class_teacher`
--
ALTER TABLE `class_teacher`
  ADD CONSTRAINT `teacher_class_id` FOREIGN KEY (`class_id`) REFERENCES `class` (`class_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teacher_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `point_log`
--
ALTER TABLE `point_log`
  ADD CONSTRAINT `point_type` FOREIGN KEY (`point_type`) REFERENCES `point_type` (`type_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `student_points`
--
ALTER TABLE `student_points`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `visit_log`
--
ALTER TABLE `visit_log`
  ADD CONSTRAINT `log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
