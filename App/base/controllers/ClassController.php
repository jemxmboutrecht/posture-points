<?php

namespace base\controllers;

use base\models;

class ClassController extends \base\models\ClassModel {

    public function __construct( int $classId = 0, string $name = '', string $nicename = '', int $mentorId = 0)
    {
        if (!empty($classId)) $this->set_classId( $classId );
        if (!empty($name)) $this->name = $name;
        if (!empty($nicename)) $this->nicename = $nicename;
        if (!empty($mentorId)) $this->set_mentorId( $mentorId );
    }

    public function get_complete_class( int $id, array $not = array() )
    {
        if (!in_array( 'class', $not ) ) $this->set_class_by_id( $id );
        if (!in_array( 'mentor', $not ) ) $this->get_mentor();
        if (!in_array( 'students', $not ) ) $this->get_students();
    }

    public function set_class_by_id( int $id = 0 )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $this->set_classId($id);

        $db->query('SELECT 
                              cl.*, 
                              co.course_id AS course_id, 
                              co.name AS course_name, 
                              co.nicename AS course_nicename
                            FROM class AS cl
                            LEFT JOIN course AS co USING(course_id) 
                            WHERE 
                              class_id = :classId');
        $db->bind(':classId', $this->get_classId());

        if ($db->execute() && $db->resultset()) {
            $result = $db->resultset()[0];
            $this->name = $result['name'];
            $this->nicename = $result['nicename'];

            $this->courseArr['id'] = $result['course_id'];
            $this->courseArr['name'] = $result['course_name'];
            $this->courseArr['nicename'] = $result['course_nicename'];

            $this->set_mentorId($db->resultset()[0]['user_id']);
        }
    }

    private function get_mentor()
    {
        $userObj = new \base\controllers\UserController();
        $userObj->get_user_by( array( 'user_id' => $this->get_mentorId() ) );
        $this->set_mentorObj($userObj);
    }

    private function get_students()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT 
                              user_id,
                              AES_DECRYPT(username, "'.AES.'") AS username,
                              AES_DECRYPT(firstname, "'.AES.'") AS firstname,
                              AES_DECRYPT(prefix, "'.AES.'") AS prefix,
                              AES_DECRYPT(lastname, "'.AES.'") AS lastname
                           FROM user WHERE user_id IN (SELECT user_id FROM class_student WHERE class_id = :classId)');
        $db->bind(':classId', $this->get_classId());

        if ($db->execute() && $db->resultset()) {
            $this->set_studentArr($db->resultset());
        }
    }
}