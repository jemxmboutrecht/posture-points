<?php

namespace base\controllers;

use base\models;

class PageController extends \base\models\PageModel
{

    /*
     * public page constructor method
     * @return void
     */
    public function __construct( $routeObj, $userObj )
    {
        \base\controllers\ApplicationController::perlog();
        $this->set_routeObj( $routeObj );

        $this->set_userObj( $userObj );

        $this->check_auth();

        $this->log_visit();

        $this->set_system_mode_style();

        if ( $this->routeObj->pageUrl == 'adminpanel' ) {
            $this->load_specPageObj( 'admin' );
            self::set_dependencies('footer', 'quill.min', 'js', 'lib', 'quilljs');
            self::set_dependencies('footer', 'quill.core', 'css', 'lib', 'quillcss');
            self::set_dependencies('footer', 'quill.snow', 'css', 'lib', 'quillsnowcss');
            self::set_dependencies('footer', 'pickmeup.min', 'js', 'lib', 'pickmeupJs');
            self::set_dependencies('footer', 'pickmeup.min', 'css', 'lib', 'pickmeupCss');
        } elseif ( $this->routeObj->pageUrl == 'ajax' ) {
            $this->load_specPageObj( 'ajax' );
            exit;
        } else {
            $this->load_specPageObj();
        }

        if ( \base\controllers\ApplicationController::STATE == 'DEVELOPMENT' ) $this->compile_sass();

        self::set_dependencies( 'header', 'fonts', 'css', 'lib/fonts', 'fonts' );

        self::set_dependencies( 'footer', 'animate.min', 'css', 'lib', 'animate' );
        self::set_dependencies( 'footer', 'all.min', 'css', 'lib/fontawesome', 'fontawesome' );
        self::set_dependencies( 'footer', 'bottom', 'css', null, 'bottom-style' );
        self::set_dependencies( 'footer', 'jquery-3.3.1.min', 'js', 'lib', 'jquery' );
        self::set_dependencies( 'footer', 'materialize.min', 'js', 'lib/materialize', 'materialize' );
        self::set_dependencies( 'footer', 'script', 'js', null, 'script', 'jquery' );

        if ( $this->routeObj->pageUrl != 'ajax' ) {
            $this->get_pageContent();

            $this->get_pageHeader();

            $this->get_pageTemplate();

            $this->get_pageFooter();
        }

        \base\controllers\ApplicationController::perlog();
    }

    /*
     * private check_auth method
     * @return void
     */
    private function check_auth( $return = false)
    {
        if ( $this->routeObj->get_current_route( 'auth' ) ) {
            if ( $this->userObj ) {
                $authLevel = $this->routeObj->get_current_route('auth');
                $userLevel = $this->userObj->get_level();
                if ( $userLevel > $authLevel ) {
                    $this->routeObj->go_to('home');
                }
            } else {
                $this->routeObj->go_to('login');
            }
        }
    }

    /*
     * private log_visit method
     * Will log the visit of the page.
     * @return void
     */
    private function log_visit()
    {
        $userId = (!empty($this->userObj) ? $this->userObj->get_userId() : NULL);
        $ip = $_SERVER['REMOTE_ADDR'];
        $page = $this->routeObj->pageUrl;
        $variables = json_encode($this->routeObj->pageVars);

        $db = \base\controllers\ApplicationController::get_db();

        $db->query('INSERT INTO visit_log (
                                            user_id, 
                                            ip, 
                                            page, 
                                            variables
                                          ) VALUES (
                                            :userId, 
                                            :ip, 
                                            :page, 
                                            :variables
                                          )');
        $db->bind(':userId', $userId);
        $db->bind(':ip', $ip);
        $db->bind(':page', $page);
        $db->bind(':variables', $variables);

        $db->execute();
    }

    /*
     * private load_specPageObj method
     * @param bool $admin
     * @return void
     */
    private function load_specPageObj( $type = false )
    {
        $menuTitle = 'default';

        if ( $type == false ) {
            $pageDir = BASEDIR . '\Page\\';
            $urlStr = ucfirst($this->routeObj->pageUrl);
            if (empty($urlStr)) $urlStr = $this->routeObj->pageTitle;
            $titleStr = $urlStr . 'Page';
            $objStr = '\Page\\';
            $this->routeObj->set_pageTitle( $urlStr );
        } elseif ( $type == 'admin' ) {
            $this->check_level( 2, 'home' );
            $pageDir = BASEDIR . '\App\admin\page\\';
            $urlStr =  ucfirst( ( $this->routeObj->pageVars['page_title'] == '' ? 'dashboard' : $this->routeObj->pageVars['page_title'] ) );
            $titleStr = $urlStr . 'Admin';
            $objStr = '\admin\page\\';

            $this->routeObj->set_pageTitle( ucfirst( $urlStr ) );

            $this->set_system_mode_style( 'admin' );
            $menuTitle = 'admin-menu';
        } elseif ( $type == 'ajax' ) {
            $pageDir = BASEDIR . '\App\ajax\\';
            $urlStr =  ucfirst( $this->routeObj->pageVars['obj'] );
            $titleStr = $urlStr . 'Ajax';
            $objStr = '\ajax\\';

            $this->routeObj->set_pageTitle( ucfirst( strtolower( $urlStr ) . 'Admin' ) );
        }

        if ($type != 'ajax') {
            $level = ($this->userObj ? $this->userObj->get_level() : 0);
            $this->set_menuObj(new \base\controllers\MenuController(null, $menuTitle, $level));
        }

        $this->load_pageObj( $pageDir, $urlStr, $titleStr, $objStr );
    }

    private function load_pageObj( string $pageDir, string $urlStr, string $titleStr, string $objStr, $default = 'DefaultPage' )
    {
        if ( file_exists( $pageDir . $urlStr . '.php' ) ) {
            $objStr .= ucfirst( $urlStr );
        } elseif ( file_exists( $pageDir . $titleStr . '.php' ) ) {
            $objStr .= ucfirst( $titleStr );
        } else {
            $objStr .= $default;
        }

        $this->set_defPageObj( new $objStr( $this ) );
    }

    private function compile_sass()
    {
        $sassDir =  BASEDIR . '\Public\src\style\scss\\';
        $cssDir =  BASEDIR . '\Public\src\style\css\\';

        $scanDir = scandir( $sassDir );

        $scssBase = new \base\lib\Scss();

        $scss = $scssBase->get_compiler();
        $scss->setFormatter('Leafo\ScssPhp\Formatter\Compressed');
        $scss->setImportPaths( $sassDir );

        foreach ( $scanDir as $file ) {
            if ( strpos( $file, '.' ) !== 0 && strpos( $file, '_' ) !== 0 && $file != 'imports' ) {
                $lastMod = ( time() - filemtime( $sassDir . $file ) ) / 60;

                if ( $lastMod < 3 || strpos( $file, 'top' ) !== false ) {
                    $sassFile = file_get_contents($sassDir . $file);
                    $cssFile = $cssDir . str_replace('scss', 'css', $file);
                    $compiled = $scss->compile($sassFile);

                    if (!empty($compiled)) {
                        file_put_contents($cssFile, $compiled);
                    }
                }
            }
        }
    }

    public function set_system_mode_style( $name = null )
    {
        $mode = ( !empty( $name ) ? $name . '_mode' : 'mode' );
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT option_value FROM system_options WHERE option_key = :mode');
        $db->bind( ':mode', $mode );

        if ($db->execute() && $db->single()) {
            $mode = $db->single()['option_value'];
        } else {
            $mode = 'default';
        }

        self::set_dependencies( 'header', 'materialize.' . $mode, 'css', null, 'materialize' );
        self::set_dependencies( 'header', 'top.' . $mode, 'css', null, 'top' );
    }

    private function get_pageContent()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM page_content WHERE page_id = (SELECT page_id FROM page WHERE url = :url) ORDER BY identifier, page_id');
        $db->bind( ':url', $this->routeObj->pageUrl );

        if ($db->execute() && $db->resultset()) {
            $contentArr = $db->resultset();
            foreach ($contentArr as $content) {
                $this->set_content($content['content'], $content['identifier']);
            }
        }
    }

    /*
     * private get_pageHeader method
     * @return void
     */
    private function get_pageHeader()
    {
        \base\controllers\ApplicationController::perlog();
        $page = $this->get_defPageObj();

        $headerFile = $this->get_templateFile( 'header' );

        include $headerFile;
        \base\controllers\ApplicationController::perlog();
    }

    /*
     * private get_pageTemplate method
     * @return void
     */
    private function get_pageTemplate()
    {
        \base\controllers\ApplicationController::perlog();
        $page = $this->get_defPageObj();

        $tmplTitles = array(
            strtolower( str_replace( ' ', '-', $this->routeObj->pageTitle ) ),
            $this->routeObj->pageUrl
        );

        if ( $this->routeObj->pageUrl == 'adminpanel' ) $dir = 'App\admin\views';
        else $dir = 'Public\views';

        $mainFile = \base\controllers\ApplicationController::load_file( 'main.phtml', $dir, $this->routeObj->pageUrl, true );

        foreach( $tmplTitles as $tmplTitle ) {
            if (!$mainFile) {
                $mainFile = \base\controllers\ApplicationController::load_file('main.phtml', $dir, $tmplTitle, true);
            }
            if (!$mainFile) {
                $mainFile = \base\controllers\ApplicationController::load_file($tmplTitle . '.phtml', $dir, null, true);
            }
        }

        if ( !$mainFile ) $mainFile = \base\controllers\ApplicationController::load_file( 'main.phtml', $dir, 'default', true );

        require_once $mainFile;
        \base\controllers\ApplicationController::perlog();
    }

    /*
     * private get_pageFooter method
     * @return void
     */
    private function get_pageFooter()
    {
        \base\controllers\ApplicationController::perlog();
        $page = $this->get_defPageObj();

        $footerFile = $this->get_templateFile( 'footer' );

        require_once $footerFile;
        \base\controllers\ApplicationController::perlog();
    }

    /*
     * private get_templateFile method
     * @param string $fileName, boolean $default
     * @return string
     */
    private function get_templateFile( string $fileName, $default = true )
    {
        \base\controllers\ApplicationController::perlog();
        $file = \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', $this->routeObj->pageUrl, true );
        if ( !$file && $default ) $file = \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', 'default', true );

        \base\controllers\ApplicationController::perlog();
        return $file;
    }

    /*
     * public get_part method
     * @param string $fileName
     */
    public function get_part( string $fileName, array $vars = array(), bool $return = false )
    {
        $file = \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'Public\views', 'parts', true );

        if ($return) {
            ob_start();
            require $file;
            $partStr = ob_get_contents();
            ob_end_clean();
            return $partStr;
        } else {
            include $file;
        }
    }

    /*
     * public svg_helper method
     * @param string $fileName
     * @return string
     */
    public function svg_helper( string $fileName )
    {
        $file = BASEDIR . '\Public\img\svg\\' . $fileName . '.svg';
        if ( file_exists( $file ) ) {
            return file_get_contents( $file );
        }
    }

    /*
     * public static get_user_title method
     * @return string
     */
    public function get_user_title()
    {
        if ( !$this->userObj || !$this->userObj->get_firstname() ) {
            return 'User';
        } else {
            return 'Hallo ' . $this->userObj->get_firstname();
        }
    }

    public function check_level( int $authLevel, string $routeName = '', array $routeVars = array() )
    {
        $check = ( $this->userObj != false ? $this->userObj->get_level() <= $authLevel : false );

        if ( !$check && empty( $routeName ) ) {
            if ( $this->routeObj->pageUrl == 'ajax' ) {
                echo '404';
                return false;
            } else {
                \base\controllers\ApplicationController::go_to( 'home' );
            }
        } else {
            return true;
        }
    }
}