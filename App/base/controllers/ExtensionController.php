<?php

namespace base\controllers;
use base\models;

class ExtensionController extends \base\models\ExtensionModel {

    public function __construct( int $id = 0, string $title = '', string $dir = '', $description = '', string $version = '0.0.0', $db = false, $active = false, string $mainFile = '', $extObj = NULL )
    {
        if ( !empty($id) ) $this->set_id( $id );
        if ( !empty($title) ) $this->set_title( $title );
        if ( !empty($dir) ) $this->set_dir( $dir );
        if ( !empty($description) ) $this->set_description( $description );

        $this->set_version( $version );

        $this->set_db( $db );
        $this->set_active( $active );

        if ( !empty($mainFile) ) $this->set_mainFile( $mainFile );
        else $this->get_mainfile_by_dir();

        if ( !empty($extObj) ) $this->set_extObj( $extObj );
        else $this->set_extObj_by_mainfile();
    }

    /*
     * public static new_ext_by_dir method
     * @param string $dir
     * @return array
     */
    public static function new_ext_by_dir( string $dir, $newObj = false )
    {
        $mainfile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $dir, true);

        $extArr = self::get_props_by_mainfile( $mainfile );

        if ( $extArr['title'] && $extArr['version'] ) {

            if ( $newObj ) {
                $extNew = new \base\controllers\ExtensionController( 0, $extArr['title'], $dir, $extArr['description'], $extArr['version'], false, false, $mainfile );

                return $extNew;
            } else {
                return $extArr;
            }

        } else {
            throw new \Exception( 'Title and version should be specified in extmain.php top comments.' );
        }
    }

    /*
     * public static set_props_by_file method
     * @param string $mainfile
     */
    public static function get_props_by_mainfile( $mainfile )
    {

        if ( file_exists( $mainfile ) ) {
            $file = file_get_contents( $mainfile );

            $docComments = array_filter( token_get_all($file), function($entry) { return $entry[0] == T_COMMENT; });
            $docComments = array_shift( $docComments )[1];

            $title = false;
            $version = false;
            $description = '';

            foreach ( explode('*', $docComments) as $commentLine ) {
                $lowComment = strtolower( $commentLine );
                if ( $commentLine != '/' && !empty($commentLine) ) {
                    if ( strpos( $lowComment, 'title' ) != false ) $title = str_replace( 'Title: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                    if ( strpos( $lowComment, 'version' ) != false ) $version = str_replace( 'Version: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                    if ( strpos( $lowComment, 'description' ) != false ) $description = str_replace( 'Description: ', '', trim( preg_replace('/\s\s+/', ' ', $commentLine ) ) );
                }
            }

            return array( 'title' => $title, 'version' => $version, 'description' => $description );

        } else {
            throw new \Exception('extmain.php not present in Extension directory');
        }

    }

    /*
     * public static set_ext_by
     * @param string $key, mixed $value
     */
    public static function set_ext_by( $key = 'id', $value )
    {
        switch ( $key ) {
            case 'id':
                $key = 'extension_id';
                break;
            case 'title':
                $key = 'extension_title';
                breka;
            case 'dir':
                $key = 'extension_dir';
                break;
        }

        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM extensions WHERE ' . $key . ' = :'.$key );
        $db->bind(':' . $key, $value);

        if ( $db->execute() && $db->resultset() ) {
            $extArr = $db->resultset()[0];
            $extObj = new ExtensionController( (int)$extArr['extension_id'], $extArr['extension_title'], $extArr['extension_dir'], $extArr['description'], $extArr['version'], true, $extArr['active']);
            return $extObj;
        } else {
            return false;
        }
    }
    /*
     * public static get_ext_by
     * @param array $extArr, string $key, mixed $value
     * @return object
     */
    public static function get_ext_by( array $extArr, string $key = 'dir', $value )
    {
        foreach ($extArr as $extension) {
            if ($extension->$key == $value) return $extension;
        }
        return false;
    }

    private function get_mainfile_by_dir()
    {
        if ( $this->dir && $mainfile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $this->dir, true) ) {
            $this->set_mainFile( $mainfile );
        }
    }

    private function set_extObj_by_mainfile()
    {
        if ( $this->mainFile ) {
            include $this->mainFile;
            $objStr = '\ext\\' . $this->dir . '\\' . $this->dir;
            $obj = new $objStr();
            $this->extObj = $obj;
        }
    }

}