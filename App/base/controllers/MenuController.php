<?php

namespace base\controllers;

use base\models;

class MenuController extends \base\models\MenuModel
{
    /*
     * public constructor method
     * @return void
     */
    public function __construct( $menuId = null, $menuTitle = '', int $level = 0 )
    {
        if ( is_int($level) ) $this->level = $level;

        if ( !empty($menuId) ) $this->get_menu( (int)$menuId, 'id' );
        elseif ( !empty($menuTitle) ) $this->get_menu( (string)$menuTitle, 'title' );
    }

    /*
     * public static add_menu_item
     * @param string $item, string $url, mixed $menu, mixed $parent
     * @return boolean
     */
    public static function add_menu_item( string $item, string $url = '', $menu = false, $parent = false)
    {
        if (empty($url)) $url = urlencode($item);

        if (!$menu) $menu = 2;
        if (is_string($menu)) $menuStr = '(SELECT menu_id FROM menu WHERE menu_title = :menu)';
        elseif (is_int($menu)) $menuStr = ':menu';

        if (!$parent) $parent = 0;
        if (is_string($parent)) {
            $db = \base\controllers\ApplicationController::get_db();
            $db->query('SELECT menu_id FROM menu_items WHERE item_title = :title OR item_url = :url');
            $db->bind(':title', $parent);
            $db->bind(':url', $parent);
        }
    }

    /*
     * private get_menu method
     * @return void, boolean
     */
    public function get_menu( $var, $type = 'title' )
    {
        if ( $type == 'title' ) $whereStr = 'menu_title = :title';
        elseif ( $type == 'id' ) $whereStr = 'menu_id = :id';

        $menuSql = 'SELECT * FROM menu WHERE ' . $whereStr;
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( $menuSql );
        $db->bind( ':' . $type, $var );

        if ( $db->execute() && $db->resultset() ) {
            $menuArr = $db->resultset()[0];
            $this->set_menuId( (int)$menuArr['menu_id'] );
            $this->set_menuTitle( $menuArr['menu_title'] );
            $this->get_menuItems_from_db();
        } else {
            //return false;
        }
    }

    /*
     * private get_menuItems method
     * @return void
     */
    private function get_menuItems_from_db()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT 
                                item_id, 
                                parent_id, 
                                item_title, 
                                item_url, 
                                level 
                            FROM 
                              menu_items 
                            WHERE 
                              menu_id = :menuId AND
                              (level >= :level OR 
                              level IS NULL)');
        $db->bind( ':menuId', $this->get_menuId() );
        $db->bind( ':level', $this->level);
        
        if ( $db->execute() && $db->resultset() ) {
            $this->format_menuItems( $db->resultset() );
        }
    }

    /*
     * private format_menuItems
     * @return array
     */
    private function format_menuItems( $itemArr )
    {
        $menuItemArr = array();
        $assocArr = array();

        foreach ( $itemArr as $item ) {
            $newItem = array(
                'title' => $item['item_title'],
                'url' => $item['item_url'],
                'level' => $item['level']
            );
            $assocArr[$item['item_title']] = $newItem;

            if ( isset( $menuItemArr[(int)$item['parent_id']] ) ) {
                $menuItemArr[(int)$item['parent_id']]['subs'][(int)$item['item_id']] = $newItem;
            } else {
                $menuItemArr[(int)$item['item_id']] = $newItem;
            }
        }
        
        $this->set_menuItems( $menuItemArr );
        $this->set_assocItems( $assocArr );
    }

}