<?php

namespace base\controllers;
use base\models;

class ApplicationController extends \base\models\ApplicationModel {

    /*
     * public constructor method
     * @param string $baseDir
     */
    public function __construct( string $baseDir )
    {
        self::perlog();
        include 'config.php';

        if ( !defined('BASEDIR' ) ) define( 'BASEDIR', $baseDir);

        $this->set_dbSettings( $dbsettings );
        $this->set_websiteSettings( $websitesettings );

        $this->set_routObj();

        $this->set_userObj();

        $pageObj = new \base\controllers\PageController( $this->get_routObj(), $this->get_userObj() );
        $this->set_pageObj( $pageObj );
        self::perlog();

        //  $this->appRouter->go_to('example', array('id' => 1, 'function' => 'test'));
    }

    /*
     * protected set_props_by_arr
     * @return void
     */
    protected function set_websiteSettings( $websiteSettings )
    {
        $this->set_siteUrl( $websiteSettings['site_url'] );
        $this->set_siteTitle( $websiteSettings['site_title'] );
        $this->set_siteSubTitle( $websiteSettings['site_subtitle'] );
        $this->set_siteDescription( $websiteSettings['site_description'] );
    }

    /*
     * public static get_db method
     * @return object
     */
    public static function get_db()
    {
        return new \base\lib\Database();
    }

    /*
     * public static go_to method
     * @return object
     */
    public static function go_to( string $routeName, $vars = array() )
    {
        \base\controllers\ApplicationController::perlog();
        include 'config.php';
        $routeObj = new \base\controllers\RouteController( $websitesettings['site_url'], true );
        $routeObj->go_to( $routeName, $vars );
        \base\controllers\ApplicationController::perlog();
    }

    /*
     * public static load_file method
     * @param string $fileStr, string $dir, string $subDir, boolean $return
     * @return mixed
     */
    public static function load_file( string $fileStr, string $dir = '', $subDir = '', $return = false )
    {
        $fileDir = BASEDIR . '\\';
        if (!empty($dir)) $fileDir .= $dir;
        if (!empty($subDir)) $fileDir .= '\\' . $subDir;

        $file = $fileDir . '\\' . $fileStr;

        if ( file_exists( $file ) ) {
            if ( $return ) return $file;
            else include $file;
        } else {
            return false;
        }
    }

    public static function shorten_text($text, $max_length = 140, $cut_off = '...', $keep_word = false)
    {
        if(strlen($text) <= $max_length) {
            return $text;
        }

        if(strlen($text) > $max_length) {
            if($keep_word) {
                $text = substr($text, 0, $max_length + 1);

                if($last_space = strrpos($text, ' ')) {
                    $text = substr($text, 0, $last_space);
                    $text = rtrim($text);
                    $text .=  $cut_off;
                }
            } else {
                $text = substr($text, 0, $max_length);
                $text = rtrim($text);
                $text .=  $cut_off;
            }
        }

        return $text;
    }

    public static function perlog()
    {
        $class = debug_backtrace()[1]['class'];
        $func = debug_backtrace()[1]['function'];

        if (!isset($GLOBALS['perlog'])) {
            $GLOBALS['perlog'] = array();
        }

        $key = $class.'->'.$func;

        if (strpos($func, '__construct') !== false) {

        }
        if (!isset($GLOBALS['perlog'][$key.'curstart'])) {

            $GLOBALS['perlog'][$key.'curstart'] = microtime(true);

        } else {
            $start = $GLOBALS['perlog'][$key.'curstart'];
            unset($GLOBALS['perlog'][$key.'curstart']);

            $runtime = number_format(((microtime(true) - $start) / 1000), 15);

            if (!isset($GLOBALS['perlog'][$key])) $GLOBALS['perlog'][$key] = array();

            $GLOBALS['perlog'][$key][] = array('start'=>$start, 'runtime' => $runtime);
        }
    }

    public static function showper()
    {
        $perlog = array_reverse($GLOBALS['perlog']);
        $returnHtml = '<table style="position: relative; background-color: #fff; z-index: 99;"><tbody>';
        foreach($perlog as $key => $per) {
            $keyArr = explode('->', $key);
            $class = $keyArr[0];
            $func = $keyArr[1];
            $returnHtml .= '<tr>';
            $returnHtml .= '<td style="border-top: 4px solid #000;background-color: orange;">Class: <strong>'.$class.'</strong></td>';
            $returnHtml .= '<td style="border-top: 4px solid #000;background-color: orange;">Function: <strong>'.$func.'</strong></td>';
            $returnHtml .= '</tr>';
            foreach ($per as $perArr) {
                $returnHtml .= '<tr>';
                $returnHtml .= '<td>Start: '.date('H:i:s', $perArr['start']).'</td>';
                $returnHtml .= '<td>Runtime: <strong>'.$perArr['runtime'].' Sec</strong></td>';
                $returnHtml .= '</tr>';
            }
        }
        $returnHtml .= '</tbody></table>';
        echo $returnHtml;
    }
}