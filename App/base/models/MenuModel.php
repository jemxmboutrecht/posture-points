<?php

namespace base\models;

class MenuModel {

    public $menuId;

    public $menuTitle;

    public $menuItems = array();

    public $assocItems = array();

    public $level;

    /*
     * public set_menuId method
     * @param int $menuId
     */
    public function set_menuId( int $menuId )
    {
        $this->menuId = $menuId;
    }

    /*
     * public get_menuId method
     * @return array
     */
    public function get_menuId()
    {
        return $this->menuId;
    }

    /*
     * public set_menuTitle method
     * @param string $menuTitle
     */
    public function set_menuTitle( string $menuTitle )
    {
        $this->menuTitle = $menuTitle;
    }

    /*
     * public get_menuTitle method
     * @return string
     */
    public function get_menuTitle()
    {
        return $this->menuTitle;
    }

    /*
     * public set_menuItems method
     * @param array $menuItems
     */
    public function set_menuItems( array $menuItems )
    {
        $this->menuItems = $menuItems;
    }

    /*
     * public get_menuItems method
     * @return array
     */
    public function get_menuItems()
    {
        return $this->menuItems;
    }

    /*
     * public set_menuItems method
     * @param array $menuItems
     */
    public function set_assocItems( array $assocItems )
    {
        $this->assocItems = array_merge($this->assocItems, $assocItems);
    }

    /*
     * public get_menuItems method
     * @return array
     */
    public function get_assocItems( string $url = '')
    {
        $return = false;

        if ($url) $return = $this->assocItems[$url];
        elseif (!$return) $return = $this->assocItems[strtolower($url)];
        else $return = $this->assocItems;

        return $return;
    }

}