<?php

namespace base\models;

class ClassModel {

    private $classId;

    public $name;

    public $nicename;

    private $studentArr = array();

    private $mentorId;

    private $mentorObj;

    public $courseArr;

    protected function set_classId( int $classId )
    {
        $this->classId = $classId;
    }

    protected function get_classId()
    {
        return $this->classId;
    }

    protected function set_studentArr( array $studentArr = array() )
    {
        $this->studentArr = array_merge($this->studentArr, $studentArr);
    }

    public function get_studentArr()
    {
        return $this->studentArr;
    }

    protected function set_mentorId( int $mentorId )
    {
        $this->mentorId = $mentorId;
    }

    protected function get_mentorId()
    {
        return $this->mentorId;
    }

    protected function set_mentorObj( object $mentorObj )
    {
        $this->mentorObj = $mentorObj;
    }

    public function get_mentorObj()
    {
        return $this->mentorObj;
    }

}