<?php

namespace base\models;

class PageModel {

    public $routeObj;

    public $userObj;

    public $menuObj;

    public $content;

    protected $defPageObj;

    private $baseUrl;

    private $roles;

    private $user;

    /*
     * protected set_routeObj method
     * @param object $routeObj
     */
    protected function set_routeObj( object $routeObj )
    {
        $this->routeObj = $routeObj;
    }

    /*
     * protected get_routeObj method
     * @return object
     */
    protected  function get_routeObj()
    {
        return $this->routeObj;
    }

    /*
     * protected set_userObj method
     * @param object,boolean $userObj
     */
    protected function set_userObj( $userObj )
    {
        $this->userObj = $userObj;
    }

    /*
     * protected get_userObj method
     * @return object
     */
    protected  function get_userObj()
    {
        return $this->userObj;
    }

    /*
     * protected set_pageUrl method
     * @param string $pageUrl
     */
    protected function set_pageUrl( string $pageUrl )
    {
        $this->pageUrl = $pageUrl;
    }

    /*
     * protected set_pageTitle method
     * @param string $pageTitle
     */
    public function set_pageTitle( string $pageTitle )
    {
        $this->pageTitle = $pageTitle;
    }

    /*
     * protected set_pageVars method
     * @param array $pageVars
     */
    protected function set_pageVars( $pageVars = array() )
    {
        $this->pageVars = $pageVars;
    }

    /*
     * protected set_menuObj method
     * @param object $menuObj
     */
    protected  function set_menuObj( object $menuObj )
    {
        $this->menuObj = $menuObj;
    }

    protected function set_content( string $content, string $key )
    {
        if ($key) {
            $this->content[$key] = $content;
        } else {
            $this->content[] = $content;
        }
    }

    /*
     * public static set_dependencies method
     * @param string $dest, string $file, string $type, string $name, string $dep
     */
    static public function set_dependencies( string $dest = 'footer', string $file, string $type, $subDir = '', string $name = null, string $dep = null )
    {
        if ( empty( $GLOBALS['dependencies'] ) ) {
            $GLOBALS['dependencies'] = array(
                'header' => array(),
                'footer' => array()
            );
        }
        if ( empty( $GLOBALS['depkeys'] ) ) {
            $GLOBALS['depkeys'] = array();
        }

        switch ( $type ) {
            case 'css':
                if ( !$subDir ) $subDir = 'css';
                $linkStr = '<link rel="stylesheet" type="text/css" href="' . BASEURL . 'Public/src/style/' . $subDir . '/[src].' . $type .'" [name]>';
                break;
            case 'js':
                $linkStr = '<script type="text/javascript" src="' . BASEURL . 'Public/src/js[sub]/[src].' . $type .'" [name]></script>';
                if ( $subDir ) $linkStr = str_replace( '[sub]', '/' . $subDir, $linkStr );
                else $linkStr = str_replace( '[sub]', '', $linkStr );
                break;
        }

        if ( isset( $linkStr ) ) {
            $linkStr = str_replace( '[src]', $file, $linkStr );

            if ( $name !== null ) {
                $linkStr = str_replace( '[name]', 'data-name="' . $name . '"', $linkStr );
                $key = $name;
            } else {
                $linkStr = str_replace( '[name]', '', $linkStr );
            }

            $key = ( !empty( $key ) ? $key : $file );

            if ( $dep ) {
                /*$arrKeysHead = array_keys( $GLOBALS['dependencies']['header'] );
                $arrSrchHead = array_search( $dep, $arrKeysHead );
                $arrKeysFoot = array_keys( $GLOBALS['dependencies']['footer'] );
                $arrSrchFoot = array_search( $dep, $arrKeysFoot );

                if ( $arrSrchHead ) array_splice($GLOBALS['dependencies']['header'], $arrSrchHead + 1, 0, $linkStr);
                elseif ( $arrSrchFoot ) array_splice($GLOBALS['dependencies']['footer'], $arrSrchFoot + 1, 0, $linkStr);
                else {
                    $GLOBALS['dependencies'][$dest][$dep] = false;
                    $newKey = array_search( $dep, $GLOBALS['dependencies'][$dest]) + 1;
                    $GLOBALS['dependencies'][$dest][$newKey] = $linkStr;
                }*/
                if ( $name ) {
                    $GLOBALS['depkeys'][$name] = $dep;
                } else {
                    throw new \Exception('Name variable should be set, when using dependency(dep) variable');
                }
            }
            $GLOBALS['dependencies'][$dest][$key] = $linkStr;
        }
    }

    /*
     * public static get_dependencies method
     * @param string $dest
     * @return string
     */
    static public function get_dependencies( string $dest )
    {
        if ($GLOBALS['depkeys']) {
            foreach($GLOBALS['depkeys'] as $name => $dep) {
                if (isset($GLOBALS['dependencies'][$dest][$name])) {
                    $depKey = array_search($dep, array_keys($GLOBALS['dependencies'][$dest]));

                    $nameArr = array($name => $GLOBALS['dependencies'][$dest][$name]);
                    unset($GLOBALS['dependencies'][$dest][$name]);

                    $firstArr = array_slice($GLOBALS['dependencies'][$dest], 0, $depKey + 1);
                    $lastArr = array_slice($GLOBALS['dependencies'][$dest], $depKey + 1);

                    $GLOBALS['dependencies'][$dest] = array_merge($firstArr, $nameArr, $lastArr);
                }
            }
        }

        if ( !empty( $GLOBALS['dependencies'] ) )  return implode( '', $GLOBALS['dependencies'][ $dest ] );
        else return null;
    }

    /*
     * protected set_defPageObj method
     * @param object $pageObj
     */
    protected function set_defPageObj( object $defPageObj )
    {
        $this->defPageObj = $defPageObj;
    }

    /*
     * protected get_defPageObj method
     * @return object
     */
    protected function get_defPageObj()
    {
        return $this->defPageObj;
    }
}