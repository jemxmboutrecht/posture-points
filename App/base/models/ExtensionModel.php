<?php

namespace base\models;

class ExtensionModel {

    public $id;

    public $title;

    public $dir;

    public $description;

    public $version;

    public $active;

    public $db;

    public $mainFile;

    public $extObj;

    /*
     * public set_id method
     * @param int $id
     */
    public function set_id( int $id )
    {
        $this->id = $id;
    }

    /*
     * public set_title method
     * @param string $title
     */
    public function set_title( string $title )
    {
        $this->title = $title;
    }

    /*
     * public set_dir method
     * @param string $dir
     */
    public function set_dir( string $dir )
    {
        $this->dir = $dir;
    }

    /*
     * public set_description method
     * @param string $description
     */
    public function set_description( string $description )
    {
        $this->description = $description;
    }

    /*
     * public set_version method
     * @param mixed $version
     */
    public function set_version( $version )
    {
        if ( !empty($version) ) $this->version = $version;
        else $this->version = '0.0.0';
    }

    /*
     * public set_db method
     * @param mixed $db
     */
    public function set_db( $db )
    {
        if ( is_int($db) ) $this->db = ( $db == 1 ? true : false );
        elseif( is_string($db) ) $this->db = ( $db == '1' ? true : false );
        elseif ( is_bool($db) ) $this->db = $db;
        else $this->db = false;
    }

    /*
     * public set_active method
     * @param mixed $active
     */
    public function set_active( $active )
    {
        if ( is_int($active) ) $this->active = ( $active == 1 ? true : false );
        elseif( is_string($active) ) $this->active = ( $active == '1' ? true : false );
        elseif ( is_bool($active) ) $this->active = $active;
        else $this->db = false;
    }

    /*
     * public set_mainFile method
     * @param string $mainFile
     */
    public function set_mainFile( string $mainFile )
    {
        $this->mainFile = $mainFile;
    }

    /*
     * public set_extObj
     * @param object $extObj
     */
    public function set_extObj( $extObj )
    {
        $this->extObj = $extObj;
    }

}