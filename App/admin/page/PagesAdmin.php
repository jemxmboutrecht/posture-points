<?php

namespace admin\page;

class PagesAdmin extends \admin\page\AdminAbstract {

    public function __construct( $pageObj )
    {
        $this->set_pageObj($pageObj);
        $pageObj->adminPageDescription = 'Pages overview.';

    }

}