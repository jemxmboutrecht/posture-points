<?php

namespace admin\page;

use base\controllers\ApplicationController;

class StudentimportAdmin extends \admin\page\AdminAbstract {

    private $importForm;
    private $formValues;

    public $formElement;

    public function __construct( $pageObj )
    {
        $this->set_pageObj($pageObj);

        $pageObj->adminPageDescription = 'Student import.';

        $this->importForm = $this->generate_importForm();
        $this->formElement = $this->importForm->generate_form();

        $this->formValues = $this->importForm->get_formValues();

        if (!empty($this->formValues)) $this->import_values();
    }

    private function generate_importForm()
    {
        $formObj = new \base\controllers\FormController('studentimport');
        $classes = $this->get_classes();

        $formObj->set_input('file', 'csv', array( 'required' => true ) );
        $formObj->set_input( 'select', 'class', array( 'values' => $classes, 'required' => true ) );
        $formObj->set_input('submit', 'import');

        return $formObj;
    }

    private function get_classes()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT class_id, nicename FROM class');

        if ($db->resultset()) {
            $returnArr = array();
            foreach ($db->resultset() as $class) {
                $returnArr[$class['class_id']] = $class['nicename'];
            }
            return $returnArr;
        } else {
            return array();
        }
    }

    private function import_values()
    {
        $studentArr = $this->map_csv( $_FILES['csv']['tmp_name'] );
        $userArr = $this->import_students( $studentArr );

        $db = \base\controllers\ApplicationController::get_db();

        foreach ($userArr as $userId) {
            $db->query('INSERT INTO class_student(class_id, user_id) VALUES (:classId, :userId)');
            $db->bind(':classId', (int)$this->formValues['class']);
            $db->bind('userId', $userId);
            $db->execute();
        }
        //var_dump($studentArr); exit;
    }

    private function map_csv( string $csvFile )
    {
        $fileStr = file_get_contents($_FILES['csv']['tmp_name']);
        $lineArr = explode(';', $fileStr);

        $fieldsArr = array('roepnaam', 'tussenvoegsel', 'achternaam', 'geboortedatum', 'studentnummer');
        $resultArr = array();

        foreach ($lineArr as $line) {
            $csvArr = str_getcsv( $line, ',');

            if (  !empty($csvArr[0]) && $csvArr[1] != 'Tussenvoegsel' ) {
                $lineArr = array_combine( $fieldsArr, $csvArr );
                $resultArr[] = $lineArr;
            }
        }

        return $resultArr;
    }

    private function import_students( array $studentsArr )
    {
        $userArr = array();

        foreach ( $studentsArr as $student ) {
            $password = 'ww'.str_replace('-', '', $student['geboortedatum']).'!';

            $userObj = new \base\controllers\UserController(
                0,
                $student['studentnummer'],
                $student['studentnummer'].'@student.mboutrecht.nl',
                $password,
                $student['roepnaam'],
                $student['tussenvoegsel'],
                $student['achternaam'],
                5
            );

            if ($userObj->register_user()) $userArr[] = $userObj->get_userId();
        }

        return $userArr;
    }
}