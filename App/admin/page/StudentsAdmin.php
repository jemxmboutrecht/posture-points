<?php

namespace admin\page;

class StudentsAdmin extends \admin\page\AdminAbstract
{

    public $studentOverview;

    public $roleOverview;

    public function __construct($pageObj)
    {
        $this->set_pageObj($pageObj);

        $this->pageObj->routeObj->set_altTitle('Studenten');
        $this->set_adminPageDescription('Studenten overzicht, pas hier individuele studenten aan.');

        $studentArr = $this->get_all_students();
        if ( $studentArr ) {
            $overviewObj = new \base\controllers\OverviewController(
                $studentArr,
                array(
                    'role_id' => null,
                    'role_title' => 'role'
                ),
                'user',
                'user_id',
                array(
                    'edit' => true,
                    'delete' => true,
                    'add' => true
                ) );
            $this->studentOverview = $overviewObj->generate_overview();
        }
    }

    private function get_all_students()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query( 'SELECT 
                                user_id,
                                AES_DECRYPT(username, "' . AES . '") AS username,
                                AES_DECRYPT(email, "' . AES . '") AS email,
                                AES_DECRYPT(firstname, "' . AES . '") AS firstname,
                                AES_DECRYPT(prefix, "' . AES . '") AS prefix,
                                AES_DECRYPT(lastname, "' . AES . '") AS lastname,
                                role_id,
                                role.title AS role_title
                            FROM user
                            LEFT JOIN user_role AS role USING(role_id)
                            WHERE user.role_id = 5' );

        if ( $db->execute() && $db->resultset() ) {
            return $db->resultset();
        } else {
            return false;
        }
    }
}
