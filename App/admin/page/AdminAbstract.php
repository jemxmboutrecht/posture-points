<?php

namespace admin\page;

abstract class AdminAbstract {

    public $pageObj;

    public $adminPageDescription;

    public $backButton;

    public function set_pageObj( object $pageObj )
    {
        $this->pageObj = $pageObj;
        if ($this->pageObj->userObj->get_level() > 0) $this->pageObj->routeObj->go_to('home');
        \base\controllers\PageController::set_dependencies('footer', 'admin', 'css');
    }

    public function set_adminPageDescription( string $description )
    {
        $this->adminPageDescription = $description;
    }

    public function set_backButton( string $url, string $text )
    {
        $this->backButton['url'] = $url;
        $this->backButton['text'] = $text;
    }
}