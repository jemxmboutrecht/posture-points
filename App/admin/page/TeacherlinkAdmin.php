<?php

namespace admin\page;

class TeacherlinkAdmin extends \admin\page\AdminAbstract
{
    private $form;

    public $formEl;

    public function __construct($pageObj)
    {
        $this->set_pageObj($pageObj);

        $this->generate_form();

        if ($this->form->get_formValues()) {
            $this->insert_link();
        }
    }

    private function generate_form()
    {
        $teachers = $this->get_teachers();
        $classes = $this->get_classes();
        $this->form = new \base\controllers\FormController( 'edit-user', 'post' );
        $this->form->set_input( 'select', 'teacher', array('label' => 'Docent', 'values' => $teachers) );
        $this->form->set_input( 'checkbox', 'classes', array('label' => 'Klassen', 'values' => $classes) );
        $this->form->set_input( 'submit', 'Link docent' );

        $this->formEl = $this->form->generate_form();
    }

    private function insert_link()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $teacherId = (int)$this->form->get_formValues('teacher');
        $classIds = $this->form->get_formValues('classes');

        foreach ($classIds as $classId) {
            $db->query('INSERT IGNORE INTO class_teacher (class_id, user_id) VALUES (:classId, :userId)');
            $db->bind(':classId', $classId);
            $db->bind(':userId', $teacherId);
            $db->execute();
        }

    }

    private function get_teachers()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT user_id, AES_DECRYPT(username, "'.AES.'") AS username FROM user WHERE role_id = 6');

        if ($result = $db->resultset()) {
            $resultArr = array();
            foreach ($result as $teacher) {
                $resultArr[$teacher['user_id']] = $teacher['username'];
            }
            return $resultArr;
        } else {
            return array();
        }
    }

    private function get_classes()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT class_id, nicename FROM class');

        if ($result = $db->resultset()) {
            $resultArr = array();
            foreach ($result as $class) {
                $resultArr[$class['class_id']] = $class['nicename'];
            }
            return $resultArr;
        } else {
            return array();
        }
    }
}