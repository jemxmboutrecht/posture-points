<?php

namespace admin\page;

class ClassAdmin extends \admin\page\AdminAbstract
{

    public $classOverview;

    public $courseOverview;

    public $studentOverview;

    public $classObj;

    public function __construct($pageObj)
    {
        $this->set_pageObj($pageObj);
        $this->pageObj->routeObj->set_altTitle('Klassen en opleidingen');
        $this->set_adminPageDescription('Hier zijn de klassen en opleidingen te beheren.');

        if (empty($this->pageObj->routeObj->get_pageVars('detail_id'))) {
            $this->get_classes();
            $this->get_courses();
        } else {
            $this->get_class();
            $this->generate_student_overview();
            $this->set_backButton(BASEURL.'adminpanel/class', 'Klassen');
        }

    }

    private function get_classes()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT 
                        cl.*, 
                        AES_DECRYPT(us.username, "'.AES.'") AS username, 
                        co.name AS coursename,
                        COUNT(cs.user_id) AS student_count
                    FROM class AS cl 
                    LEFT JOIN user AS us USING(user_id) 
                    LEFT JOIN course AS co USING(course_id) 
                    LEFT JOIN class_student AS cs USING(class_id)
                    GROUP BY cl.class_id');

        if ($db->execute()) {
            $this->generate_class_overview($db->resultset());
        } else {
            $this->classOverview = 'Geen klassen gevonden.';
        }
    }

    private function get_courses()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM course');

        if ($db->execute()) {
            $this->generate_course_overview($db->resultset());
        } else {
            $this->courseOverview = 'Geen opleidingen gevonden.';
        }
    }

    private function get_class()
    {
        $classObj = new \base\controllers\ClassController();
        $classObj->get_complete_class((int)$this->pageObj->routeObj->get_pageVars('detail_id'));
        $this->classObj = $classObj;
    }

    private function generate_class_overview( $classArr )
    {
        $overviewObj = new \base\controllers\OverviewController(
            $classArr,
            array(
                'name' => 'Klascode',
                'nicename' => 'Afkorting',
                'class_id' => null,
                'user_id' => null,
                'course_id' => null,
                'username' => 'mentor',
                'coursename' => 'opleiding',
                'student_count' => 'studenten'
            ),
            'class',
            'class_id',
            array(
                'add' => true,
                'edit' => true,
                'delete' => true,
                'idurl' => BASEURL.'adminpanel/class',
                'idLabel' => 'name',
                'idTitle' => 'klas'));
        $this->classOverview = $overviewObj->generate_overview();
    }

    private function generate_course_overview( $courseArr )
    {
        $overviewObj = new \base\controllers\OverviewController(
            $courseArr,
            array(
                'course_id' => null,
                'name' => 'Naam opleiding',
                'nicename' => 'Afkorting',
            ),
            'course',
            'course_id',
            array(
                'add' => true,
                'edit' => true,
                'delete' => true,
                'idLabel' => 'name',
                'idTitle' => 'opleiding')
        );
        $this->courseOverview = $overviewObj->generate_overview();
    }

    private function generate_student_overview()
    {
        $overviewObj = new \base\controllers\OverviewController(
            $this->classObj->get_studentArr(),
            array(
                'user_id' => null,
                'firstname' => 'voornaam',
                'prefix' => 'tussenvoegsel',
                'lastname' => 'achternaam'
            ),
            'students',
            'user_id',
            array());
        $this->studentOverview = $overviewObj->generate_overview();
    }

}