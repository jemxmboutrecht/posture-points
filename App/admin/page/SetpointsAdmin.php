<?php

namespace admin\page;

use base\controllers\ApplicationController;

class SetpointsAdmin extends \admin\page\AdminAbstract
{
    private $form;

    public $pointform;

    public function __construct($pageObj)
    {
        $this->set_pageObj($pageObj);

        \base\controllers\PageController::set_dependencies('footer', 'points', 'css', null, 'pointstyle');
        \base\controllers\PageController::set_dependencies('footer', 'points', 'js', null, 'points', 'jquery');

        $this->point_form();

        if ($this->form->get_formValues()) $this->set_points();
    }

    private function point_form()
    {
        $classes = $this->get_classes();

        $this->form = new \base\controllers\FormController('set-points');
        $this->form->set_input('checkbox', 'classes', array( 'label' => 'Klassen<br>', 'values' => $classes ) );
        $this->form->set_input('text', 'firstname', array( 'label'=> 'Voornaam', 'classes' => 'admin' ) );
        $this->form->set_input('hidden', 'userid' );
        $this->form->set_input('number', 'points', array( 'label'=> 'Punten', 'required' => true ) );
        $this->form->set_input('number', 'max', array( 'label' => 'Maximaal', 'required' => true ) );
        $this->form->set_input('submit', 'Set points');

        $this->pointform = $this->form->generate_form();
    }

    private function get_classes()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT class_id, nicename FROM class');

        if ($db->resultset()) {
            $returnArr = array();
            foreach ($db->resultset() as $class) {
                $returnArr[$class['class_id']] = $class['nicename'];
            }
            return $returnArr;
        } else {
            return array();
        }
    }

    private function set_points()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $classes = $this->form->get_formValues('classes', false);
        $userIds = ( !empty($this->form->get_formValues('userid', false)) ? explode(',', $this->form->get_formValues('userid')) : array() );
        $points = $this->form->get_formValues('points');
        $max = $this->form->get_formValues('max');

        if (!empty($classes)) {
            $db->query('SELECT user_id FROM class_student WHERE class_id IN ('.implode(',',$classes).')');

            if ($db->resultset()) {
                foreach ($db->resultset() as $userId) {
                    if (!in_array($userId['user_id'], $userIds)) {
                        $userIds[] = $userId['user_id'];
                    }
                }
            }
            sort($userIds);
        }

        foreach ($userIds as $userId) {
            $db->query('INSERT INTO 
                            student_points (
                                user_id, 
                                points, 
                                maximum
                            ) VALUES (
                                :userId,
                                :points,
                                :maximum)
                            ON DUPLICATE KEY UPDATE
                                points = :points,
                                maximum = :maximum');
            $db->bind(':userId', $userId);
            $db->bind(':points', $points);
            $db->bind(':maximum', $max);
            $db->execute();
        }
    }
}