<?php

namespace admin\page;

class DashboardAdmin extends \admin\page\AdminAbstract {

    public function __construct( $pageObj )
    {
        $this->set_pageObj($pageObj);
        $pageObj->adminPageDescription = 'Standard overview of the system statistics.';

    }

}