<?php

namespace ext\BareRows\inc;

class Uninstall
{

    public $uninstallLog = '';

    public $error = false;

    public function __construct()
    {
        $this->uninstallLog .= 'Uninstall started...<br><hr>';
        $this->remove_tables();
    }

    private function remove_tables()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $this->uninstallLog .= '<hr>Removing tables... <br><br>';

        // Dropping br_options table
        try {
            $db->query('DROP TABLE br_options' );

            if ( $db->execute() ) {
                $this->uninstallLog .= 'br_options table succefully deleted.<br>';
            }
        } catch ( Exception $e ) {
            $this->uninstallLog .= 'Something went wrong when deleting br_options.<br>';
            $this->error = true;
        }

        // Dropping br_columns table
        try {
            $db->query('DROP TABLE br_columns' );

            if ( $db->execute() ) {
                $this->uninstallLog .= 'br_columns table succefully deleted.<br>';
            }
        } catch ( Exception $e ) {
            $this->uninstallLog .= 'Something went wrong when deleting br_columns.<br>';
            $this->error = true;
        }

        // Dropping br_rows table
        try {
            $db->query('DROP TABLE br_rows' );
            $db->execute();
            $this->uninstallLog .= 'br_rows table succefully deleted.<br>';
        } catch ( Exception $e ) {
            $this->uninstallLog .= 'Something went wrong when deleting br_rows.<br>';
            $this->error = true;
        }

        $this->uninstallLog .= '<br><br>Done removing tables... <hr>';

    }
}