CREATE TABLE br_rows (
  row_id INT(11) NOT NULL AUTO_INCREMENT,
  page_id INT(11) NOT NULL,
  options VARCHAR(255) NULL,
  PRIMARY KEY (row_id)
) ENGINE = InnoDB;

CREATE TABLE br_columns (
  col_id INT(11) NOT NULL AUTO_INCREMENT,
  row_id INT(11) NOT NULL,
  title VARCHAR(150) NULL,
  name VARCHAR(150) NOT NULL,
  bg_type ENUM("image","color") NOT NULL DEFAULT "color",
  bg_value VARCHAR(100) NOT NULL DEFAULT "white",
  content TEXT NULL,
  options VARCHAR(255) NULL,
  PRIMARY KEY (col_id)
) ENGINE = InnoDB;

CREATE TABLE br_options (
  option_id INT(11) NOT NULL AUTO_INCREMENT,
  parent_id INT(11) NULL,
  br_key VARCHAR(30) NOT NULL,
  br_value VARCHAR(100) NOT NULL,
  PRIMARY KEY (option_id)
) ENGINE = InnoDB;

ALTER TABLE br_rows
ADD CONSTRAINT page_id FOREIGN KEY (page_id)
REFERENCES page(page_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE br_columns
ADD CONSTRAINT row_id FOREIGN KEY (row_id)
REFERENCES br_rows(row_id) ON DELETE CASCADE ON UPDATE CASCADE;