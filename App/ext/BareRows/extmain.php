<?php
/*
 * Title: Bare Rows
 * Version: 0.0.1
 * Description: With this extension you can easily add rows and columns to a page.
 */

namespace ext\BareRows;

use ext;

class BareRows extends \ext\ExtensionAbstract {

    public function __construct()
    {
        //var_dump(debug_backtrace()[1]);

    }

    public function install_extension()
    {
        $install = new \ext\BareRows\inc\Install();
        return array($install->error, $install->installLog);
    }

    public function uninstall_extension()
    {
        $uninstall = new \ext\BareRows\inc\Uninstall();
        return array($uninstall->error, $uninstall->uninstallLog);
    }

    public function admin_content()
    {
        \base\controllers\PageController::set_dependencies('footer', 'admin-brstyle', 'css', '', 'Admin BareRows', NULL );
        \base\controllers\PageController::set_dependencies('footer', 'admin-brscript', 'js', '', 'Admin BareRows', 'jquery' );

        $options    = $this->get_br_options();
        $optionStr  = \ext\ExtensionAbstract::get_ext_part('optionoverview', 'BareRows', $options, true);

        return $optionStr;
    }

    /*
     * public static get_br_options method
     * @return array
     */
    public static function get_br_options()
    {

        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM br_options');

        if ( $db->execute() && $db->resultset() ) {
            return self::format_options( $db->resultset() );
        } else {
            return false;
        }
    }

    /*
     * public static format_options method
     * @param array $optionArr
     * @return array
     */
    public static function format_options( array $optionArr )
    {
        $newOptionArr = array();

        foreach ( $optionArr as $option ) {
            $newArr = array('key' => $option['br_key'], 'label' => $option['br_label'], 'value' => $option['br_value'] );
            if ( !empty($option['parent_id']) ) {
                $newOptionArr[$option['parent_id']]['sub'][$option['option_id']] = $newArr;
            } else {
                $newOptionArr[$option['option_id']] = $newArr;
            }
        }

        return $newOptionArr;
    }

}