<?php

namespace ext;

abstract class ExtensionAbstract {

    abstract public function install_extension();
    abstract public function uninstall_extension();
    abstract public function admin_content();

    public static function get_extension( string $dir, string $action )
    {
        var_dump($title); exit;
    }

    public static function get_ext_part( $fileName, $dir, $var, $return = false )
    {
        if ( $return ) ob_start();

        include \base\controllers\ApplicationController::load_file( $fileName . '.phtml', 'App\ext', $dir . '\parts', true );

        if ( $return ) {
            $returnStr = ob_get_clean();

            ob_clean();

            return $returnStr;
        }
    }

}