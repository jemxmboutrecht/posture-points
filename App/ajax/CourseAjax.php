<?php

namespace ajax;

class CourseAjax extends \ajax\AjaxAbstract
{

    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 0);
    }

    static protected function add_course()
    {
        $formObj = new \base\controllers\FormController( 'add-course', 'post', array( 'ajax' => array( 'obj' => 'course', 'func' => 'insert_course', 'callback' => 'reload' ) ) );
        $formObj->set_input('text', 'name', array( 'required' => true, 'attributes' => array( 'max' => 50 ) ) );
        $formObj->set_input('text', 'nicename', array( 'required' => true ) );
        $formObj->set_input('submit', 'Voeg opleiding toe' );
        echo $formObj->generate_form();
    }

    static protected function insert_course($name, $niceName)
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('INSERT INTO course (
                                        name, 
                                        nicename
                                      ) VALUES (
                                        :name, 
                                        :niceName
                                      )');
        $db->bind(':name', $name);
        $db->bind(':niceName', $niceName);

        if ($db->execute() && $db->lastInsertId()) {
            echo 1;
        } else {
            echo 0;
        }
    }

    static protected function edit_course( $courseId )
    {
        $courseArr = self::get_course_by_id( $courseId );
        $formObj = new \base\controllers\FormController( 'edit-course', 'post', array( 'ajax' => array( 'obj' => 'course', 'func' => 'update_course', 'callback' => 'reload' ) ) );
        $formObj->set_input('hidden', 'course_id', array( 'value'=>$courseId ) );
        $formObj->set_input('text', 'name', array( 'value' => $courseArr['name'], 'required' => true, 'attributes' => array( 'max' => 50 ) ) );
        $formObj->set_input('text', 'nicename', array( 'value' => $courseArr['nicename'], 'required' => true ) );
        $formObj->set_input('submit', 'Wijzig opleiding' );
        echo $formObj->generate_form();
    }

    static protected function update_course( $courseId, $name, $niceName )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('UPDATE course SET
                                        name = :name, 
                                        nicename = :niceName
                                      WHERE
                                        course_id = :courseId');
        $db->bind(':name', $name);
        $db->bind(':niceName', $niceName);
        $db->bind(':courseId', $courseId);

        if ($db->execute()) echo 1;
        else echo 0;
    }

    static protected function get_course_by_id( $courseId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM course WHERE course_id = :courseId');
        $db->bind(':courseId', $courseId);

        if ($db->execute() && $db->resultset()) {
            return $db->resultset()[0];
        } else {
            echo 0;
        }
    }

    static protected function delete_course( $courseId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('DELETE FROM course WHERE course_id = :courseId');
        $db->bind(':courseId', $courseId);

        if ($db->execute()) echo 1;
        else echo 0;
    }

}