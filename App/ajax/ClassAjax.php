<?php

namespace ajax;

use base\controllers\ClassController;

class ClassAjax extends \ajax\AjaxAbstract
{

    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 0);
    }


    static protected function add_class()
    {
        $teachers = self::get_teachers();
        $courses = self::get_courses();
        $formObj = new \base\controllers\FormController( 'add-class', 'post', array( 'ajax' => array( 'obj' => 'class', 'func' => 'insert_class', 'callback' => 'reload' ) ) );
        $formObj->set_input('text', 'name', array( 'required' => true, 'attributes' => array( 'max' => 50 ) ) );
        $formObj->set_input('text', 'nicename', array( 'required' => true ) );
        $formObj->set_input( 'select', 'user_id', array('values' => $teachers) );
        $formObj->set_input( 'select', 'course_id', array('values' => $courses) );
        $formObj->set_input('submit', 'Voeg klas toe' );
        echo $formObj->generate_form();
    }

    static protected function insert_class($name, $niceName, $userId, $courseId)
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('INSERT INTO class (
                                        name, 
                                        nicename, 
                                        user_id,
                                        course_id
                                      ) VALUES (
                                        :name, 
                                        :niceName, 
                                        :userId,
                                        :courseId
                                      )');
        $db->bind(':name', $name);
        $db->bind(':niceName', $niceName);
        $db->bind(':userId', $userId);
        $db->bind(':courseId', $courseId);

        if ($db->execute() && $classId = $db->lastInsertId()) echo 1;
        else echo 0;
    }

    static protected function edit_class( $classId )
    {
        $classArr = self::get_class_by_id( $classId );
        $teachers = self::get_teachers();
        $courses = self::get_courses();
        $formObj = new \base\controllers\FormController( 'add-class', 'post', array( 'ajax' => array( 'obj' => 'class', 'func' => 'update_class', 'callback' => 'reload' ) ) );
        $formObj->set_input('hidden', 'class_id', array('value'=>$classId));
        $formObj->set_input('text', 'name', array( 'value' => $classArr['name'], 'required' => true, 'attributes' => array( 'max' => 50 ) ) );
        $formObj->set_input('text', 'nicename', array( 'value' => $classArr['nicename'], 'required' => true ) );
        $formObj->set_input( 'select', 'user_id', array('values' => $teachers, 'selected' => $classArr['user_id']) );
        $formObj->set_input( 'select', 'course_id', array('values' => $courses, 'selected' => $classArr['course_id']) );
        $formObj->set_input('submit', 'Wijzig klas' );
        echo $formObj->generate_form();
    }

    static protected function update_class( $classId, $name, $niceName, $userId, $courseId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('UPDATE class SET
                                        name = :name, 
                                        nicename = :niceName, 
                                        user_id = :userId,
                                        course_id = :courseId
                                      WHERE class_id = :classId
                                      ');
        $db->bind(':name', $name);
        $db->bind(':niceName', $niceName);
        $db->bind(':userId', $userId);
        $db->bind(':courseId', $courseId);
        $db->bind(':classId', $classId);

        if ($db->execute()) echo 1;
        else echo 0;
    }

    static protected function delete_class( $classId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('DELETE FROM class WHERE class_id = :classId');
        $db->bind(':classId', $classId );

        if ($db->execute()) echo 1;
        else echo 0;
    }

    static protected function get_class_by_id( $classId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT cl.* FROM class AS cl WHERE class_id = :classId ');
        $db->bind(':classId', $classId);

        if ($db->execute() && $db->resultset()) {
            return $db->resultset()[0];
        }


    }

    static protected function get_teachers()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT user_id, AES_DECRYPT(username, "'.AES.'") AS username FROM user WHERE role_id = 6');

        if ($db->execute() && $db->resultset()) {
            $returnArr = array();
            foreach ($db->resultset() as $teacher) {
                $returnArr[$teacher['user_id']] = $teacher['username'];
            }
            return $returnArr;
        } else {
            return array();
        }
    }

    static protected function get_courses()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT course_id, name FROM course');

        if ($db->execute() && $db->resultset()) {
            $returnArr = array();
            foreach ($db->resultset() as $teacher) {
                $returnArr[$teacher['course_id']] = $teacher['name'];
            }
            return $returnArr;
        } else {
            return array();
        }
    }


}