<?php

namespace ajax;

class RoleAjax extends \ajax\AjaxAbstract
{

    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 0);
    }

    static protected function edit_role( $roleId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $sanitize = new \base\lib\Sanitize();
        $roleId = $sanitize->input($roleId, array('INT', 'UTF8'));

        $db->query('SELECT * FROM user_role WHERE role_id = :roleId' );
        $db->bind(':roleId', $roleId );

        if ( $db->execute() && $db->resultset() ) {
            self::generate_edit_form( $db->resultset()[0] );
        } else {
            echo 'Something went wrong! Try again later.';
        }
    }

    static protected function generate_edit_form( $roleArr )
    {
        $formObj = new \base\controllers\FormController( 'edit-role', 'post', array( 'ajax' => array( 'obj' => 'role', 'func' => 'update_role', 'callback' => 'reload' ) ) );
        $formObj->set_input('hidden', 'role-id', array( 'value' => $roleArr['role_id'] ) );
        $formObj->set_input('text', 'title', array( 'required' => true, 'value' => $roleArr['title'], 'attributes' => array( 'maxlength' => 32 ) ) );
        $formObj->set_input('textarea', 'description', array( 'value' => $roleArr['description'], 'attributes' => array( 'maxlength' => 255 ) ) );
        $formObj->set_input('number', 'level', array( 'required' => true, 'value' => $roleArr['level'], 'attributes' => array( 'min' => 0, 'max' => 50, 'step' => 1 ) ) );
        $formObj->set_input('submit', 'Edit role' );
        echo $formObj->generate_form();
    }

    static protected function update_role( $roleId, $title, $description, $level )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $db->query('UPDATE user_role SET `title` = :title, `description` = :description, `level` = :level WHERE role_id = :roleId');
        $db->bind(':title', $title);
        $db->bind(':description', $description);
        $db->bind(':level', $level);
        $db->bind('roleId', $roleId);

        if ( $db->execute() ) return true;
        else return false;
    }

    static protected function delete_role( $roleId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $sanitize = new \base\lib\Sanitize();
        $roleId = $sanitize->input($roleId, array('INT', 'UTF8'));

        $db->query( 'DELETE FROM user_role WHERE role_id = :roleId' );
        $db->bind( ':roleId', $roleId );

        if ( $db->execute() ) {
            return true;
        } else {
            return false;
        }
    }

    static protected function add_role( )
    {
        $formObj = new \base\controllers\FormController( 'add-role', 'post', array( 'ajax' => array( 'obj' => 'role', 'func' => 'insert_role', 'callback' => 'reload' ) ) );
        $formObj->set_input('text', 'title', array( 'required' => true, 'attributes' => array( 'maxlength' => 32 ) ) );
        $formObj->set_input('textarea', 'description', array( 'attributes' => array( 'maxlength' => 255 ) ) );
        $formObj->set_input('number', 'level', array( 'required' => true, 'attributes' => array( 'min' => 0, 'max' => 50, 'step' => 1 ) ) );
        $formObj->set_input('submit', 'Add role' );
        echo $formObj->generate_form();
    }

    static protected function insert_role( $title, $description, $level )
    {
        $sanitize = new \base\lib\Sanitize();
        $title = ( $title ? $sanitize->input( $title, array( 'STRING', 'UTF8' ) ) : false );
        $level = ( $level ? $sanitize->input( $level, array( 'INT', 'UTF8' ) ) : false );

        $db = \base\controllers\ApplicationController::get_db();

        $db->query('INSERT INTO user_role( `title`, `description`, `level` ) VALUES ( :title, :description, :levelInt )');
        $db->bind( ':title', $title );
        $db->bind( ':description', $description );
        $db->bind( ':levelInt', $level );

        if ( $db->execute() && $db->lastInsertId() ) echo '1';
        else echo '0';
    }
}