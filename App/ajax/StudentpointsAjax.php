<?php

namespace ajax;

use base\lib\Sanitize;

class StudentpointsAjax extends \ajax\AjaxAbstract
{

    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 2);
    }

    static protected function get_students( $input )
    {
        $db = \base\controllers\ApplicationController::get_db();

        if (!empty($input)) $havingStr = 'HAVING firstname LIKE :input';
        else $havingStr = '';

        if (strpos($input, ' ') !== false) {
            $inputArr = explode(' ', $input);
            $input = $inputArr[0];
            if (count($inputArr) > 2) {
                $prefix = $inputArr[1];
                $lastname = $inputArr[2];
            } else {
                $prefix = '';
                $lastname = $inputArr[1];
            }
            $havingStr .= ' AND lastname LIKE :lastname AND prefix LIKE :prefix';
        }

        $classes = self::get_linked_students();

        if ($classes) $classStr = 'AND sp.user_id IN ('.implode(',', $classes).') ';
        else $classStr = '';

        $db->query('SELECT 
                            us.user_id,
                            CAST(AES_DECRYPT(firstname, "'.AES.'") AS CHAR(255)) firstname, 
                            CAST(AES_DECRYPT(prefix, "'.AES.'") AS CHAR(255)) prefix, 
                            CAST(AES_DECRYPT(lastname, "'.AES.'") AS CHAR(255)) lastname,
                            points, 
                            maximum,
                            cl.nicename AS class  
                           FROM user AS us
                           LEFT JOIN student_points AS sp ON us.user_id = sp.user_id
                           LEFT JOIN class_student AS cs ON sp.user_id = cs.user_id
                           LEFT JOIN class AS cl ON cs.class_id = cl.class_id 
                           WHERE role_id = 5
                           '.$classStr.'
                           '.$havingStr.'
                           ORDER BY firstname');

        if (!empty($input)) $db->bind(':input', $input.'%');
        if (isset($lastname)) {
            $db->bind(':prefix', $prefix.'%');
            $db->bind(':lastname', $lastname.'%');
        }

        if ($db->resultset()) {
            $resultStr = '';
            $fileName = \base\controllers\ApplicationController::load_file( 'pointblock.phtml', 'Public\views', 'parts', true );

            $buttons = self::get_point_types();

            foreach ($db->resultset() as $user) {
                $nicename = $user['firstname'];
                $nicename .= (!empty($user['prefix']) && $user['prefix'] !== ' '?' '.$user['prefix']:'');
                $nicename .= ' '.$user['lastname'];

                $resultStr .= self::get_part_string('pointblock', array('nicename'=>$nicename,'user'=>$user,'buttons'=>$buttons, 'class'=>$user['class']));
            }
            echo $resultStr;
        } else {
            echo $db->queryError();
        }
    }

    static protected function get_count( $userIds )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $sanitize = new \base\lib\Sanitize();

        $today = date('Y-m-d');
        $start = $today.' 00:00:00';
        $end = $today.' 23:59:59';

        $userIds = $sanitize->input($userIds, array('STRING', 'UTF8'));

        $db->query('SELECT 
                        student_id, 
                        COUNT(logtime) AS count 
                    FROM point_log 
                    WHERE student_id IN ('.$userIds.') 
                    AND logtime BETWEEN :start AND :end 
                    GROUP BY student_id');
        $db->bind(':start', $start);
        $db->bind(':end', $end);

        if ($db->resultset()) {
            $returnArr = array();
            foreach ($db->resultset() as $count) {
                $returnArr[$count['student_id']] = (int)$count['count'];
            }
            echo json_encode($returnArr);
            $obj = new \base\controllers\UserController();
        }
    }

    static protected function get_linked_students()
    {
        $user = new \base\controllers\UserController();
        $user->set_user_by_session();

        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT user_id FROM class_student WHERE class_id IN (SELECT class_id FROM class_teacher WHERE user_id = :userId)');
        $db->bind(':userId', $user->get_userId());

        if ($result = $db->resultset()) {
            $resultArr = array();
            foreach ($result as $student) {
                $resultArr[] = $student['user_id'];
            }
            return $resultArr;
        } else {
            return false;
        }
    }

    static protected function point_form($type, $point, $userid, $points, $max)
    {
        $formObj = new \base\controllers\FormController(
            'point-registration',
            'post',
            array(
                'ajax' => array(
                    'obj' => 'Studentpoints',
                    'func' => 'register_point',
                    'callback' => '(function(d){ bflb( null, d, null, reload()); })'
                )
            )
        );
        $formObj->set_input('text', 'description', array( 'label' => 'Reden' ) );
        $formObj->set_input('hidden', 'type', array( 'value' => $type ) );
        $formObj->set_input('hidden', 'point', array( 'value' => $point ) );
        $formObj->set_input('hidden', 'userid', array( 'value' => $userid ) );
        $formObj->set_input('hidden', 'points', array( 'value' => $points ) );
        $formObj->set_input('hidden', 'max', array( 'value' => $max ) );
        $formObj->set_input('submit', 'Punt registreren' );

        echo $formObj->generate_form();
    }

    static protected function register_point( $description, $type, $point, $userid, $points, $max )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $sanitize = new \base\lib\Sanitize();

        $user = new \base\controllers\UserController();
        $user->set_user_by_session();

        $userid = $sanitize->input( $userid, array( 'INT', 'UTF8' ) );
        $type = $sanitize->input( $type, array( 'INT', 'UTF8' ) );
        $point = $sanitize->input( $point, array( 'INT', 'UTF8' ) );
        $description = $sanitize->input( $description, array( 'UTF8' ) );

        $db->query('INSERT INTO point_log(
                                            logtime, 
                                            teacher_id, 
                                            student_id,
                                            point_type,
                                            point, 
                                            description
                                        ) VALUES (
                                            NOW(),
                                            :teacherId,
                                            :studentId,
                                            :pointType,
                                            :point,
                                            :description
                                        )');
        $db->bind(':teacherId', $user->get_userId() );
        $db->bind(':studentId', $userid );
        $db->bind(':pointType', $type );
        $db->bind(':point', $point );
        $db->bind(':description', $description );

        if ($db->execute()) {
            $points = $points + $point;

            if ($points > $max) $points = $max;

            $db->query('UPDATE student_points SET points = :points WHERE user_id = :userId');
            $db->bind(':points', $points);
            $db->bind(':userId', $userid);

            if ($db->execute()) echo 'Punt is geregistreerd!';
            else echo 'Er is iets fout gegaan. #pr-2';
        } else {
            echo 'Er is iets fout gegaan. #pr-1';
        }
    }

    static protected function edit_form( $time )
    {
        $typesArr = self::get_point_types( true );
        $log = self::get_log( $time );

        $formObj = new \base\controllers\FormController(
            'edit-log',
            'post',
            array(
                'ajax' => array(
                    'obj' => 'Studentpoints',
                    'func' => 'edit_log',
                    'callback' => '(function(d){ bflb( null, d, null, reload()); })'
                ),
                'attributes' => array(
                    'data-points' => $typesArr['json']
                )
            )
        );

        $formObj->set_input('text', 'description', array( 'label' => 'Reden', 'value' => $log['description'] ) );
        $formObj->set_input('select', 'type', array( 'values' => $typesArr['types'], 'selected' => $log['point_type'] ) );
        $formObj->set_input('hidden', 'point', array( 'value' => $log['point'] ) );
        $formObj->set_input('hidden', 'oldpoint', array( 'value' => $log['point'] ) );
        $formObj->set_input('hidden', 'points', array( 'value' => $log['points'] ) );
        $formObj->set_input('hidden', 'max', array( 'value' => $log['max'] ) );
        $formObj->set_input('hidden', 'studentId', array( 'value' => $log['student_id'] ) );
        $formObj->set_input('hidden', 'time', array( 'value' => $time ) );
        $formObj->set_input('submit', 'Punt aanpassen' );

        echo $formObj->generate_form();
        echo '<button data-time="'.$time.'" data-point="'.$log['point'].'" data-points="'.$log['points'].'" data-id="'.$log['student_id'].'" class="danger" id="delete-point"><i class="fa fa-trash"></i>Verwijderen</button>';
    }

    static protected function edit_log( $description, $type, $point, $oldpoint, $points, $max, $studentId, $time )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('UPDATE point_log SET point_type = :type, point = :point, description = :description WHERE logtime = :time');
        $db->bind(':type', $type);
        $db->bind(':point', $point);
        $db->bind(':description', $description);
        $db->bind(':time', str_replace('T', ' ', $time));

        if ($db->execute()) {
            if ($point != $oldpoint) {
                $oldPoints = (int)$points;
                $points = $points + ($oldpoint * -1);
                $points = $points + $point;

                if ($points > $max) $points = (int)$max;

                if ($oldPoints != $points) {
                    $db->query('UPDATE student_points SET points = :points WHERE user_id = :userId');
                    $db->bind(':points', $points);
                    $db->bind(':userId', $studentId);
                    if ($db->execute()) {
                        echo 'Het aanpassen is gelukt!';
                    } else {
                        echo 'Er is iets fout gegaan. #L-1';
                    }
                } else {
                    echo 'Het aanpassen is gelukt!';
                }
            } else {
                echo 'Het aanpassen is gelukt!';
            }
        } else {
            echo 'Er is iets fout gegaan. #L-2';
        }
    }

    static protected function delete_point($time, $point, $points, $userId)
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('DELETE FROM point_log WHERE logtime = :time');
        $db->bind(':time', $time);

        $points = $points + ($point * -1);

        if ($db->execute()) {
            $db->query('UPDATE student_points SET points = :points WHERE user_id = :userId');
            $db->bind(':points', $points);
            $db->bind(':userId', $userId);

            if ($db->execute()) echo 1;
            else echo 0;
        } else {
            echo 0;
        }
    }

    static protected function get_log( $time )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $logtime = str_replace('T', ' ', $time);

        $db->query('SELECT 
                                pl.*, 
                                sp.points AS points,
                                sp.maximum AS max                                
                            FROM point_log AS pl 
                            LEFT JOIN student_points AS sp ON pl.student_id = sp.user_id
                            WHERE logtime = :logtime');
        $db->bind(':logtime', $logtime );

        if ($db->resultset()) {
            return $db->resultset()[0];
        }
    }

    static protected function get_point_types( bool $select = false )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM point_type');

        if ($db->resultset()) {
            if (!$select) {
                $resultStr = '';
                foreach ($db->resultset() as $type) {
                    $resultStr .= self::get_part_string('pointtypebutton', array('type' => $type));
                }
                return $resultStr;
            } else {
                $typeArr = array();
                $jsonArr = array();
                foreach ($db->resultset() as $type) {
                    $typeArr[$type['type_id']] = '<i class="fa fa-'.$type['icon'].'"></i>'.$type['name'].' ('.$type['point'].')';
                    $jsonArr[$type['type_id']] = $type['point'];
                }
                return array('types'=>$typeArr,'json'=>str_replace('"', '\'', json_encode($jsonArr) ) );
            }
        }
    }

    static protected function get_type()
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('SELECT * FROM point_type WHERE type_id = :typeId');
        $db->bind(':typeId', $type);

        if ($db->resultset()) {

        }
    }
}