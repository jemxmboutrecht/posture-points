<?php

namespace ajax;

class ExtensionAjax extends \ajax\AjaxAbstract
{

    private $newExt;

    public function __construct($pageObj)
    {
        $this->init_ajaxFunc($pageObj, 0);
    }

    static protected function install_extension( $installVar )
    {
        $extFile = \base\controllers\ApplicationController::load_file('extmain.php', 'App\ext', $installVar, true);

        $dir = $installVar;

        if ( $extFile ) {

            include $extFile;

            $extObjStr = '\ext\\' . $dir . '\\' . $dir;
            $extObj = new $extObjStr();
            $installArr = $extObj->install_extension();

            echo $installArr[1];

            if (!$installArr[0]) self::add_extension($dir, $extFile, $extObj);
        } else {
            echo 'No "extmain.php" file found or no directory by the name: "' . $installVar . '" found';
        }

    }


    static protected function add_extension( $dir, $extFile, $extObj )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $extObj = new \base\controllers\ExtensionController(0, '', $dir, '', '', false, false, $extFile, $extObj );
        $propsArr = $extObj->get_props_by_mainfile( $extFile );
        $extObj->set_title($propsArr['title']);
        $extObj->set_version($propsArr['version']);
        $extObj->set_description($propsArr['description']);

        $db->query('SELECT extension_id FROM extensions WHERE extension_dir = :dir');
        $db->bind(':dir', $dir);

        if ( $db->execute() && !$db->single() ) {
            $db->query('INSERT INTO extensions(
                                            extension_title, 
                                            extension_dir, 
                                            description,
                                            version, 
                                            active
                                          ) VALUES (
                                            :title,
                                            :dir,
                                            :description,
                                            :version,
                                            :active )');
            $db->bind(':title', $extObj->title);
            $db->bind(':dir', $extObj->dir);
            $db->bind(':description', $extObj->description);
            $db->bind(':version', $extObj->version);
            $db->bind(':active', $extObj->active);

            if ($db->execute() && $db->lastInsertId()) {
                echo '<hr>Extension added under id: ' . $db->lastInsertId() . '. You can activate it now!<hr>';
            } else {
                echo '<hr>Something went wrong when adding the extension to the dedicated table.<hr>';
            }
        } else {
            echo '<hr>Extension already exists in the database.<hr>';
        }
    }


    /*
     * protected static uninstall_extension method
     * @param int $extId
     * @return void
     */
    protected static function uninstall_extension( int $extId )
    {
        $extObj = \base\controllers\ExtensionController::set_ext_by('id', $extId );
        $uninstallArr = $extObj->extObj->uninstall_extension();

        echo $uninstallArr[1];
        if (!$uninstallArr[0]) self::remove_extension( $extId );
    }

    /*
     * protected static remove_extension method
     * @param int $extId
     * @return void
     */
    protected static function remove_extension( int $extId )
    {
        $db = \base\controllers\ApplicationController::get_db();

        $db->query('DELETE FROM extensions WHERE extension_id = :id');
        $db->bind(':id', $extId);

        if ($db->execute()) {
            echo '<hr>Extension removed from database.<hr>';
        } else {
            echo '<hr>< ! >Something went wrong! Extension was not removed from database.<hr>';
        }
    }

    protected static function activate_extension( $extensionId )
    {
        $db = \base\controllers\ApplicationController::get_db();
        $db->query('UPDATE extensions SET active = 1 WHERE extension_id = :id' );
        $db->bind(':id', $extensionId);
        if ($db->execute()) {
            echo '1';
        } else {
            echo '0';
        }
    }

}